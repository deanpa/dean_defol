#!/bin/bash
#SBATCH -J defol_0
#SBATCH -A landcare00025
#SBATCH --mail-type=end
#SBATCH --mail-user=andersond@landcareresearch.co.nz
#SBATCH --time=480:00:00
#SBATCH --mem-per-cpu=3000  
#SBATCH -o job-%j.%N.out
#SBATCH -e job-%j.%N.err

source /landcare/sw/osgeo/osgeo.sh

srun startDefol.py --basicdata=$DEFOLPROJDIR/out_basicdata_0.pkl



