#!/usr/bin/env python


import os
import numpy as np

#import matplotlib
#matplotlib.use('Agg') # Must be before importing matplotlib.pyplot or pylab!
#import matplotlib.pyplot as P

import pylab as P
from scipy.stats.mstats import mquantiles
import prettytable
import pickle

class ResultsProcessing(object):
    def __init__(self, gibbsobj, basicdata):

######################
######################
######################
#        self.defolpath = os.getenv('DEFOLPROJDIR', default='.')
#        self.resultDatFile = os.path.join(self.defolpath,'summaryTable_defol.txt')
######################
######################
######################

        self.basicdata = basicdata
        self.gibbsobj = gibbsobj
        self.ndat = len(self.gibbsobj.siggibbs)
    
#        print(np.shape(self.gibbsobj.rhogibbs))

#        self.rhoYR = np.array([2, 22, 53, 78, 90])
#        self.rhoTrace = self.gibbsobj.rhogibbs[:, self.rhoYR]  
#        print(np.shape(self.rhoTrace))
 
#        self.rhoYrMeans = np.round(np.mean(self.gibbsobj.rhogibbs, axis = 0), 4)
#        self.rhoAllMeans = np.round(np.mean(self.gibbsobj.rhogibbs, axis = 1), 4)

        self.errTraceID = np.array([5, 25, 50, 95])
        self.errByYear = self.gibbsobj.errByYeargibbs
        self.errTrace = self.errByYear[:, self.errTraceID]


        self.results = np.hstack([self.gibbsobj.B0gibbs, 
                                self.gibbsobj.B1gibbs,
                                self.gibbsobj.B2gibbs,                    
                                np.expand_dims(self.gibbsobj.kgibbs, 1), 
                                self.gibbsobj.gammagibbs,
                                np.expand_dims(self.gibbsobj.siggibbs,1),
                                np.expand_dims(self.gibbsobj.delta0gibbs, 1),
                                self.gibbsobj.popgibbs,
                                self.errTrace])

        self.npara = self.results.shape[1]
#        print(self.npara)
#        print(np.shape(self.gibbsobj.B0gibbs))
#        print(np.shape(self.gibbsobj.B1gibbs))
#        print(np.shape(self.gibbsobj.B2gibbs))

#        self.makeTableFX()

#    @staticmethod    
    def quantileFX(self):
        return mquantiles(self.results, prob=[0.025, 0.5, 0.975], axis = 0)
    
    def makeTableFX(self):
        resultTable = np.zeros(shape = (4, self.npara))
        resultTable[0:3, :] = np.round(self.quantileFX(), 5)
        resultTable[3, :] = np.round(np.mean(self.results, axis = 0), 5) 
        resultTable = resultTable.transpose()
        aa = prettytable.PrettyTable(['Names', 'Low CI', 'Median', 'High CI', 'Mean'])

        self.names = (['a0_B0', 'a1_B0', 'a2_B0', 'a3_B0', 'a0_B1', 'a1_B1', 'a2_B1', 
                'a3_B1', 'a0_B2', 'a1_B2',
                'a2_B2', 'a3_B2', 'k parameter', 'Gamma_MinWinTmp', 'Gamma_PcpJune', 'Sigma', 
                'delta0', 'PopRast34 (1)', 
                'PopPred32 (1)', 'PopRast20 (0)', 'PopPred18 (0)', 'ErrDetect1', 
                'ErrDetect2', 'ErrDetect3', 'ErrDetect4'])
        for i in range(self.npara):
            name = self.names[i]
            row = [name] + resultTable[i].tolist()
            aa.add_row(row)
        print(aa)

        self.summaryTable = np.round(resultTable.copy(), 5)


    def getErrSummary(self):
        """
        get mean and 95% CI for err detection by year
        """
        self.nYears = self.basicdata.nYears
        self.years = np.arange(self.nYears, dtype = int) + 1912
        self.errSumStats = np.zeros((4, self.nYears))
        self.errSumStats[0] = np.mean(self.errByYear, axis = 0)
        self.errSumStats[1:3] = mquantiles(self.errByYear, prob=[.025, 0.975], axis=0)
#        print('sum Err', self.errByYear[25, :50])

    def plotFX(self):
        cc = 0
        nfigures = np.int(np.ceil(self.npara/6.0))
#        print('nfigures, self.npara', nfigures, self.npara)
        for i in range(nfigures):
            P.figure(i)
            lastFigure = i == (nfigures - 1)
#            print(lastFigure)
            for j in range(6):
                P.subplot(2,3,j+1)
                #print cc
                if cc < self.npara:
                    P.plot(self.results[0:self.ndat, cc])
                    P.title(cc)
                    P.title(str(cc) + ') ' + self.names[cc])
                cc = cc + 1
                if cc == (nfigures * 6):
#                    Years = 1912 + np.arange(len(self.rhoYrMeans))
                    P.plot(self.years, self.errSumStats[0])
                    P.plot(self.years, self.errSumStats[1])
                    P.plot(self.years, self.errSumStats[2])
                    P.title('Detect error by year')
            P.show(block=lastFigure)

    @staticmethod    
    def r2FX(pop, pred):
        ssRes = np.sum((pop - pred)**2)
        meanPop = np.mean(pop)
        ssTot = np.sum((pop - meanPop)**2)
        r2 = 1 - (ssRes/ssTot)
        return r2
    

    def plotPredFX(self):
        """
        plotting fx to assess pop pred againt pop and defoliation
        """
        # set years and associated masks for pop and pred data
        # population
        rastMask = self.basicdata.maskBool.copy()
        popMask = rastMask[2:]
        popDat = self.basicdata.popRast[2:]
        popDat2 = popDat[popMask]

        predMask = rastMask[2:]
        predDat = self.basicdata.popPred.copy()
        predDat2 = predDat[predMask]

#        beta2 = self.basicdata.B2_rast[70]
#        beta2 = beta2[self.basicdata.betaMask == 1]
#        b2_Mu = self.basicdata.B2_Mu[self.basicdata.betaMask == 1]
#        r2 = self.r2FX(popDat2, predDat2)
#        print('###########  r2  #############', r2)

        defolDat = self.basicdata.defol[predMask]
        ndefol = len(defolDat)
        defolJitter = np.random.uniform(-.25, .25, ndefol)
        defolDat2 = defolDat + defolJitter
        P.figure()
        P.subplot(2,2,1)
        P.scatter(popDat2, predDat2)

        P.subplot(2,2,2)
        P.scatter(popDat2, defolDat2)

        P.subplot(2,2,3)
        P.scatter(predDat2, defolDat2)

#        P.subplot(2,2,4)
#        P.scatter(b2_Mu, beta2)
        P.show()



########            Write data to file
########
    def writeToFileFX(self):
        (m, n) = self.summaryTable.shape

        # create new structured array with columns of different types
        structured = np.empty((m,), dtype=[('Names', 'a12'), ('Low CI', np.float),
                    ('Median', np.float), ('High CI', np.float), ('Mean', np.float)])
        # copy data over
        structured['Low CI'] = self.summaryTable[:, 0]
        structured['Median'] = self.summaryTable[:, 1]
        structured['High CI'] = self.summaryTable[:, 2]
        structured['Mean'] = self.summaryTable[:, 3]
#        names = ['PopGr_mean', 'PopGr_Clim1', 'PopGr_Clim2', 'DD_mean', 'DD_Clim1', 'DD_Clim2', 
#                'DDD_mean', 'DDD_Clim1', 'DDD_Clim2', 'SpatialEffect_k', 'MinWinTemp', 
#                'Pcp_June', 'Sigma', 'Sigma', 'sigmaDetect', 'delta0', 'Rho']
        structured['Names'] = self.names
        np.savetxt('summaryTable_Defol_M1.txt', structured, fmt=['%s', '%.4f', '%.4f', '%.4f', '%.4f'],
                    header='Names Low_CI Median High_CI Mean')



def main():

    # paths and data to read in
    defolpath = os.getenv('DEFOLPROJDIR', default='.')

    inputGibbs = os.path.join(defolpath, 'out_gibbs_0.pkl')
    fileobj = open(inputGibbs, 'rb')
    gibbsobj = pickle.load(fileobj)
    fileobj.close()

    inputGibbs = os.path.join(defolpath, 'out_basicdata_0.pkl')
    fileobj = open(inputGibbs, 'rb')
    basicdata = pickle.load(fileobj)
    fileobj.close()

    resultsobj = ResultsProcessing(gibbsobj, basicdata)

    resultsobj.makeTableFX()

    resultsobj.getErrSummary()



    
    resultsobj.plotFX()

    resultsobj.plotPredFX()
    
#    resultsobj.writeToFileFX()


if __name__ == '__main__':
    main()


