#!/usr/bin/env python
import os
import paramsNoDetect as params
from scipy import stats
#from scipy.special import gammaln
from scipy.special import gamma
import numpy as np
import numpy.ma as ma
import rasterutils
from numba import jit, int64
import pickle
from lcrimageutils import mdl


def logit(x):
    """
    Function (logit) to convert probability to real number
    """
    return np.log(x) - np.log(1 - x)

def inv_logit(x):
    """
    Function to convert real number to probability
    """
    return np.exp(x) / (1 + np.exp(x))


def gamma_pdf(xx, shape, scale):
    """
    pdf for gamma distribution
    example:    gamma_pdf(arg1, shape, 1/scale)
    """
    gampdf = 1.0 / (scale**shape) / gamma(shape) * xx**(shape - 1) * np.exp(-(xx/scale))
    return np.log(gampdf)


@jit
def errorDistOLD(nYr, winsize, halfwinsize, RHO):
    """
    3-d raster of weights with a different rho for each year
    """
    errdistarray = np.empty((nYr, winsize, winsize))   #, numpy.int)
    for i in range(nYr):
        rhoI = RHO[i]
        for x in range(winsize):
            for y in range(winsize):
                distx = (x - halfwinsize)
                disty = (y - halfwinsize)
                dist = np.sqrt(distx*distx + disty*disty)
                if dist == 0:
                    errdistarray[i, y, x] = 0.0
                else:
                    errdistarray[i, y, x] = 1/(dist**rhoI)
    return errdistarray

@jit
def errorDist(winsize, halfwinsize, RHO, errdistarray):
    """
    3-d raster of weights with a different rho for each year
    """
#    errdistarray = np.empty((winsize, winsize))   #, numpy.int)
    for x in range(winsize):
        for y in range(winsize):
            distx = (x - halfwinsize)
            disty = (y - halfwinsize)
            dist = np.sqrt(distx*distx + disty*disty)
            if dist == 0:
                errdistarray[y, x] = 0.0
            else:
                errdistarray[y, x] = 1/(dist**RHO)
    return errdistarray

@jit
def dnormPDF(arr1, mn, precalc, denomcalc):
        diff = (arr1 - mn)
        numcalc = diff*diff
        normpdf = precalc * np.exp(-numcalc/denomcalc)
        return normpdf


@jit
def calcNeighboursErr(nyears, rastMask, err_distarray, halfwinsize, rastShape,
            nNeighbour):
    """
    return number of neighbours and initial mean neighbourhood error from the input raster and mask
    Run once in Basicdata
    """
#    tmpRast = rastMask[0]
#    nNeighbour = np.ones_like(rastMask) 
    xsize = rastShape[1]  # errRast.shape
    ysize = rastShape[0]  # errRast.shape
    for i in range(nyears):
        mask = rastMask[i]
#        nNeighRast = nNeighbour[i]
#        err_distarrayYR =err_distarray[i]
        for x in range(xsize):
            for y in range(ysize):
                if mask[y, x] != 0:
                    # offset into dist array - deal with top and left edges
                    xdistoff = 0
                    ydistoff = 0
                    # top left x
                    tlx = x - halfwinsize
                    if tlx < 0:
                        xdistoff = -tlx
                        tlx = 0
                    tly = y - halfwinsize
                    if tly < 0:
                        ydistoff = -tly
                        tly = 0
                    brx = x + halfwinsize
                    if brx > xsize - 1:
                        brx = xsize - 1
                    bry = y + halfwinsize
                    if bry > ysize - 1:
                        bry = ysize - 1
                    nNeigh = 1
                    for cx in range(tlx, brx+1):
                        for cy in range(tly, bry+1):
                            if mask[cy, cx] != 0:
                                pixelY = ydistoff + cy - tly
                                pixelX = xdistoff + cx - tlx
                                dist = err_distarray[pixelY, pixelX]
                                if dist != 0:
                                    nNeigh += 1
#                    nNeighRast[y, x] = nNeigh
                    nNeighbour[i, y, x] = nNeigh
#        nNeighbour[i] = nNeighRast
    return(nNeighbour)



@jit
def updateErrorByYear(nYears, errRast, dbernNow, dbernNew, errRast_s, 
                    mask, distarray, halfwinsize, precalc, 
                    denomcalc, rastShape):
    """
    return a detection error raster se1 from the input raster and mask
    Makes use of distance (see errorDist)
    """
    xsize = rastShape[1]  # errRast.shape
    ysize = rastShape[0]  # errRast.shape

    for i in range(nYears):
        for x in range(xsize):
            for y in range(ysize):
                if mask[(i+2), y, x] != 0:
                    # offset into dist array - deal with top and left edges
                    xdistoff = 0
                    ydistoff = 0
                    # top left x
                    tlx = x - halfwinsize
                    if tlx < 0:
                        xdistoff = -tlx
                        tlx = 0
                    tly = y - halfwinsize
                    if tly < 0:
                        ydistoff = -tly
                        tly = 0
                    brx = x + halfwinsize
                    if brx > xsize - 1:
                        brx = xsize - 1
                    bry = y + halfwinsize
                    if bry > ysize - 1:
                        bry = ysize - 1
                    errD = errRast[i, y, x]                                    # err at x,y
                    errD_s = errRast_s[i, y, x]                                # proposed err at x,y
                    sumarray = 0.0
                    eArray = 0.0
                    for cx in range(tlx, brx+1):
                        for cy in range(tly, bry+1):
                            if mask[(i+2), cy, cx] != 0:
                                pixelY = ydistoff + cy - tly
                                pixelX = xdistoff + cx - tlx
                                distWT = distarray[pixelY, pixelX]
                                if distWT != 0:
                                    sumarray += distWT
                                    eInvDist = errRast[i, cy, cx] * distWT
                                    eArray += eInvDist
                    if sumarray == 0.0:
                        mu_errDetect = 0.0
                    else:    
                        mu_errDetect = eArray / sumarray
                    dbernNowXY = dbernNow[i, y, x]
                    dbernNewXY = dbernNew[i, y, x]
                    precalcXY = precalc[i, y, x]
                    denomcalcXY = denomcalc[i, y, x]
                    priorNow = np.log(dnormPDF(errD, mu_errDetect, precalcXY, denomcalcXY))
                    priorNew = np.log(dnormPDF(errD_s, mu_errDetect, precalcXY, denomcalcXY))
                    pnow = dbernNowXY + priorNow
                    pnew = dbernNewXY + priorNew
                    rValue = np.exp(pnew - pnow)
                    zValue = np.random.random()
                    # if proposed parameter is better, keep it and update
                    if rValue > zValue:
                        errRast[i, y, x] = errD_s
    return(errRast)




#                sumErrYR += errRast[y, x]
#                nCells += 1   
#    errRast = (errRast - (sumErrYR / nCells)) * mask       # zero sum in YR i
#    tauRast = np.exp(delta0 + errRast)
#    pObsRast = 1 - np.exp(-(expPopRast * tauRast)) 
#####    return(errRast)



@jit
def muError(nyears, errRast, rastMask, distarray, halfwinsize, precalc, denomcalc, 
            precalc_s, denomcalc_s, mu_errDetectRast, rastShape):
    """
    return the mu error raster for updating sigmaDetection
    Makes use of distance (see errorDist)
    """
    xsize = rastShape[1]  # errRast.shape
    ysize = rastShape[0]  # errRast.shape
    NowDNorm = 0.0
    NewDNorm = 0.0
    for i in range(nyears):
#        mask = rastMask[i]
#        precalcYR = precalc[i]
#        denomcalcYR = denomcalc[i]
#        precalcYR_s = precalc_s[i]
#        denomcalcYR_s = denomcalc_s[i]
#        errRastYR = errRast[i]
#        distarrayYR = distarray[i]
        for x in range(xsize):
            for y in range(ysize):
                if rastMask[i, y, x] != 0:
                    # offset into dist array - deal with top and left edges
                    xdistoff = 0
                    ydistoff = 0
                    # top left x
                    tlx = x - halfwinsize
                    if tlx < 0:
                        xdistoff = -tlx
                        tlx = 0
                    tly = y - halfwinsize
                    if tly < 0:
                        ydistoff = -tly
                        tly = 0
                    brx = x + halfwinsize
                    if brx > xsize - 1:
                        brx = xsize - 1
                    bry = y + halfwinsize
                    if bry > ysize - 1:
                        bry = ysize - 1
                    errD = errRast[i, y, x]                                    # err at x,y
                    sumarray = 0.0
                    eArray = 0.0
                    sumarray = 0.0
                    eInvDist = 0.0
#                    cc = 0.0
                    for cx in range(tlx, brx+1):
                        for cy in range(tly, bry+1):
                            if rastMask[i, cy, cx] != 0:
                                pixelY = ydistoff + cy - tly
                                pixelX = xdistoff + cx - tlx
                                distWT = distarray[pixelY, pixelX]
                                if distWT != 0:
                                    sumarray += distWT                         # sum of weight_i (denominator)
                                    eInvDist = errRast[i, cy, cx] * distWT      # error_i * weight_i
                                    eArray += eInvDist                          # sum or err*wt (numerator)
                    if sumarray == 0.0:
                        mu_errDetect = 0.0
                    else:    
                        mu_errDetect = eArray / sumarray

                    mu_errDetectRast[i, y, x] = mu_errDetect
                    precalcXY = precalc[i, y, x]
                    denomcalcXY = denomcalc[i, y, x]
                    precalcXY_s = precalc_s[i, y, x]
                    denomcalcXY_s = denomcalc_s[i, y, x]
                    NowDNorm += np.log(dnormPDF(errD, mu_errDetect, precalcXY, denomcalcXY))
                    NewDNorm += np.log(dnormPDF(errD, mu_errDetect, precalcXY_s, denomcalcXY_s))
    return (mu_errDetectRast, NowDNorm, NewDNorm)


@jit
def rho_MuError(rho, rho_s, nyears, errRast, rastMask, distarray, distarray_s, halfwinsize, 
                precalc, denomcalc, mu_errDetectRast, priorNow, priorNew, rastShape, 
                mu_errDetectRast_s):
    """
    use proposed rho values to get proposed mu errors and update rhos accordingly
    """
#    tmpRast = rastMask[0]
    xsize = rastShape[1]  # errRast.shape
    ysize = rastShape[0]  # errRast.shape
    for i in range(nyears):
#        mu_errDetectRast_s = np.empty((ysize, xsize))
        NowDNorm = 0.0
        NewDNorm = 0.0
#        mask = rastMask[i]
#        precalcYR = precalc[i]
#        denomcalcYR = denomcalc[i]
#        errRastYR = errRast[i]
#        distarrayYR_s = distarray_s[i]
        for x in range(xsize):
            for y in range(ysize):
                if rastMask[(i+2), y, x] != 0:
#                if mask[y, x] != 0:
                    # offset into dist array - deal with top and left edges
                    xdistoff = 0
                    ydistoff = 0
                    # top left x
                    tlx = x - halfwinsize
                    if tlx < 0:
                        xdistoff = -tlx
                        tlx = 0
                    tly = y - halfwinsize
                    if tly < 0:
                        ydistoff = -tly
                        tly = 0
                    brx = x + halfwinsize
                    if brx > xsize - 1:
                        brx = xsize - 1
                    bry = y + halfwinsize
                    if bry > ysize - 1:
                        bry = ysize - 1
                    errD = errRast[i, y, x]                                    # err at x,y
                    sumarray = 0.0
                    eArray = 0.0
                    sumarray = 0.0
                    eInvDist = 0.0
                    for cx in range(tlx, brx+1):
                        for cy in range(tly, bry+1):
                            if rastMask[(i+2), cy, cx] != 0:
                                pixelY = ydistoff + cy - tly
                                pixelX = xdistoff + cx - tlx
                                distWT = distarray_s[pixelY, pixelX]
                                if distWT != 0:
                                    sumarray += distWT                         # sum of weight_i (denominator)
                                    eInvDist = errRast[i, cy, cx] * distWT      # error_i * weight_i
                                    eArray += eInvDist                          # sum or err*wt (numerator)
                    if sumarray == 0.0:
                        mu_errDetect_s = 0.0
                    else:    
                        mu_errDetect_s = eArray / sumarray
                    mu_errDetectRast_s[i, y, x] = mu_errDetect_s
                    mu_errDetect = mu_errDetectRast[i, y, x] 
                    precalcXY = precalc[i, y, x]
                    denomcalcXY = denomcalc[i, y, x]
                    NowDNorm += np.log(dnormPDF(errD, mu_errDetect, precalcXY, denomcalcXY))
                    NewDNorm += np.log(dnormPDF(errD, mu_errDetect_s, precalcXY, denomcalcXY))
#    return (mu_errDetectRast, rho, distarray)
    return (mu_errDetectRast_s, NowDNorm, NewDNorm)




@jit
def rho_MuError_OLD(rho, rho_s, nyears, errRast, rastMask, distarray, distarray_s, halfwinsize, 
                precalc, denomcalc, mu_errDetectRast, priorNow, priorNew, rastShape):
    """
    use proposed rho values to get proposed mu errors and update rhos accordingly
    """
#    tmpRast = rastMask[0]
    xsize = rastShape[1]  # errRast.shape
    ysize = rastShape[0]  # errRast.shape
    for i in range(nyears):
        mu_errDetectRast_s = np.empty((ysize, xsize))
        NowDNorm = 0.0
        NewDNorm = 0.0
        rhoI_s = rho_s[i]
        mask = rastMask[i]
        precalcYR = precalc[i]
        denomcalcYR = denomcalc[i]
        errRastYR = errRast[i]
        distarrayYR_s = distarray_s[i]
        for x in range(xsize):
            for y in range(ysize):
                if mask[y, x] != 0:
                    # offset into dist array - deal with top and left edges
                    xdistoff = 0
                    ydistoff = 0
                    # top left x
                    tlx = x - halfwinsize
                    if tlx < 0:
                        xdistoff = -tlx
                        tlx = 0
                    tly = y - halfwinsize
                    if tly < 0:
                        ydistoff = -tly
                        tly = 0
                    brx = x + halfwinsize
                    if brx > xsize - 1:
                        brx = xsize - 1
                    bry = y + halfwinsize
                    if bry > ysize - 1:
                        bry = ysize - 1
                    errD = errRastYR[y, x]                                    # err at x,y
                    sumarray = 0.0
                    eArray = 0.0
                    sumarray = 0.0
                    eInvDist = 0.0
                    for cx in range(tlx, brx+1):
                        for cy in range(tly, bry+1):
                            if mask[cy, cx] != 0:
                                pixelY = ydistoff + cy - tly
                                pixelX = xdistoff + cx - tlx
                                distWT = distarrayYR_s[pixelY, pixelX]
                                if distWT != 0:
                                    sumarray += distWT                         # sum of weight_i (denominator)
                                    eInvDist = errRastYR[cy, cx] * distWT      # error_i * weight_i
                                    eArray += eInvDist                          # sum or err*wt (numerator)
                    if sumarray == 0.0:
                        mu_errDetect_s = 0.0
                    else:    
                        mu_errDetect_s = eArray / sumarray
                    mu_errDetectRast_s[y, x] = mu_errDetect_s
                    mu_errDetect = mu_errDetectRast[i, y, x] 
                    precalcXY = precalcYR[y, x]
                    denomcalcXY = denomcalcYR[y, x]
                    NowDNorm += np.log(dnormPDF(errD, mu_errDetect, precalcXY, denomcalcXY))
                    NewDNorm += np.log(dnormPDF(errD, mu_errDetect_s, precalcXY, denomcalcXY))
        pnow = NowDNorm + priorNow[i]
        pnew = NewDNorm + priorNew[i]
        rValue = np.exp(pnew - pnow)
        zValue = np.random.random()
        # if proposed parameter is better, keep it and update
        if rValue > zValue:
            rho[i] = rhoI_s               
            mu_errDetectRast[i] = mu_errDetectRast_s
            distarray[i] = distarrayYR_s
    return (mu_errDetectRast, rho, distarray)


class BasicData(object):
    def __init__(self, defolFname, climateFname, params, landCovFname):
        """
        Object to read in defol-weather data, and climate data from PCA
        Create initial pop raster for 1910 - 2003.
        We estimate '10-'11 because of delayed density dependence (AR2 model)
        """
        self.params = params        
        ##############
        ###############      Initial values parameters for updating
        # ############       Use values from params in case don't use pickled initial values
        # k is decay parameter for spatial effect
        self.k = self.params.k
        # make dist array for neighbourhood window incorporating k
        self.se_distarray = rasterutils.makeDistArray(self.k,
                                self.params.se_winsize, self.params.se_halfwinsize)
        # alphas for b0 - intrinsic rate of growth
        self.alpha_b0 = self.params.alpha_b0.copy()                # intercept and ax1 only
        # alphas for b1 - density dependence
        self.alpha_b1 = self.params.alpha_b1.copy()             # intercept, ax1, and ax2
        # alphas for b2 - delayed density dependence
        self.alpha_b2 = self.params.alpha_b2.copy()
        # gamma parameter for weather variables
        self.gamma = self.params.gamma.copy()

        # sigma variance parameter of population size
        self.sigma = self.params.sigma
        self.sqrt_sigma = self.params.sqrt_sigma

        # add to se to take log
        self.logPlus = self.params.logPlus          # 0.001
        
        # initial detection parameters
#        self.delta0 = self.params.delta0
#        self.rho = self.params.rho                                  # specify below
#        self.sigmaDetect = self.params.sigmaDetect
#        self.errdistarrayZero = np.empty((self.params.se_winsize, self.params.se_winsize)) 

        #################
        #################

        # read in defoliation and weather data
        self.defolWeath = np.genfromtxt(defolFname,  delimiter=',', names=True,
            dtype=['i8', 'i8', 'i8', 'i8', 'f8', 'f8', 'i8', 'i8',
                'i8', 'f8', 'f8', 'f8'])
        # read in climate data
        self.climate = np.genfromtxt(climateFname,  delimiter=',', names=True,
            dtype=['f8', 'f8', 'i8', 'i8', 'f8', 'f8', 'f8', 'f8', 'f8', 
                'f8', 'f8', 'f8', 'f8'])
        # read in landcover data
        self.landCovDat = np.genfromtxt(landCovFname,  delimiter=',', names=True,
            dtype=['f8', 'f8', 'i8', 'i8', 'f8', 'f8', 'f8', 'f8', 'f8','f8', 'f8', 'f8', 
            'f8', 'f8','f8', 'f8', 'f8', 'f8', 'f8','f8', 'f8', 'f8', 'f8', 'f8',
            'f8', 'f8', 'f8', 'f8'])    

        self.defolMaskOutArrays()
        self.makeMask()                                     # mask of cells to analyse
        self.makeDefolWeath()                               # multiple 3-d arrays of defol and weather data
        self.makePopRast()                                  # 3-d array of population data
        self.proposeNewPop()
        
        # 3-d array of spatial effects
        self.spatialEffectRast = self.spatialEffectFX(self.se_distarray)
#        self.spatialEffectPsi = self.spatialEffectRast
        
        # 4-d array for climate variables ax1 and ax2 for B0, B1, and B2
        # no updating of this
        (self.b0_climateVariables, self.b1_climateVariables, 
            self.b2_climateVariables) = self.makeClimateRast()                            

        # make 3-d arrays of Betas
        # have to update when updata alphas
        # intrinsic rate of growth
        self.B0_rast = self.makeBetaRasters(self.alpha_b0, self.b0_climateVariables)
        # density dependence
        self.B1_rast = self.makeBetaRasters(self.alpha_b1, self.b1_climateVariables)
        # delayed density dependence
        self.B2_rast = self.makeBetaRasters(self.alpha_b2, self.b2_climateVariables)
        
        #calculate the density dependence effect
        self.densityDepend = self.calcDensityDepend(self.B1_rast, self.popRast[1:-1])

        #calculate the DELAYED density dependence effect
        self.delayedDensityDepend = self.calcDelayedDensityDepend(self.B2_rast, self.popRast[0:-2])


        # 3 or 4-d array of weather variables multiplied times gamma
        # one stack for each variable
        self.calcWeatherGamma()

        # one 3-d array of total weather effect (sum of calcWeatherGamma)
        self.weatherEffect = self.calcWeatherEffect(self.weatherGamma)

        # make 3-d predicted population
        self.popPred = self.PopPredict(self.B0_rast, 
                        self.densityDepend,
                        self.delayedDensityDepend, 
                        self.spatialEffectRast,              #Psi,
                        self.weatherEffect)
    
        # make probability of detection array and detection error array
###        self.makeInitialTauArray()

#        self.errorDistArray = errorDist(self.params.se_winsize, self.params.se_halfwinsize, 
#                self.rho, self.errdistarrayZero)

#        self.wrapNeighbourRastFX()

#        (self.precalc, self.denomcalc) = self.calcDNormSD(self.sigmaDetect)

#        self.mu_errDetectRast = np.zeros_like(self.popPred)



#        tmpMask10 = self.rastMask[10]
#        tmpMask90 = self.rastMask[90]
#        rasterutils.writeToFile(tmpMask10, 'mask10.img')
#        rasterutils.writeToFile(tmpMask90, 'mask90.img')





    #########################################
    #########################################
    #
    #   Below are functions for Basicdata
    #
    ########################################
    ########################################

    def defolMaskOutArrays(self):
        """
        get defol and covariate data from 1912 - 2010
        rm minn from years 1953 and earlier
        """ 
        # mask of years before 1954
        earlyYearsMask = self.defolWeath['year_'] < 1954
        # mask of data not in original data that went up to 2003
        maskPID = self.defolWeath['pid'] == -1        
        # remove early years (<1912) and minnesota before 1954
        maskRmEarlyMinn = earlyYearsMask & maskPID
        self.maskRmEarlyMinn = np.invert(maskRmEarlyMinn)
        # mask out data
        easting = self.defolWeath['x']
        northing = self.defolWeath['y']
        easting = easting[self.maskRmEarlyMinn]
        northing = northing[self.maskRmEarlyMinn]
        self.yearsDefolDat = self.defolWeath['year_']
        self.yearsDefolDat = self.yearsDefolDat[self.maskRmEarlyMinn]
        self.defolAll = self.defolWeath['defol']
        # mask out defol data
        self.defolAll = self.defolAll[self.maskRmEarlyMinn]
        ### get columns and rows
        (self.col, self.row) = rasterutils.getColsAndRows(easting, northing)
        self.years = np.arange(1912, 2011)   # np.unique(self.defolWeath['year_'])
        self.nYears = np.size(self.years)
        self.seqYears = np.arange(self.nYears)
        self.nTotDat = len(easting)
        # 2-d shape of rasters
        self.rastShape = [rasterutils.N_YPIX, rasterutils.N_XPIX]
        self.seqI = np.arange(2, self.nYears)
        self.ndat = len(easting)
#        self.rho = np.ones(self.nYears)

    def makeMask(self):
        """
        Function to make 3-d mask of cells to analyse: ones and zeros
        Make a mask for each year.
        Note that 1910-1953 have Ontario only. Post '53 have MINN and ONT
        """
        self.rastMask = np.zeros(((self.nYears + 2), rasterutils.N_YPIX, 
            rasterutils.N_XPIX), dtype=np.uint8)
        self.zerosRaster = self.rastMask[0]
        # will be used to propose new pop values
        self.datID = np.empty(self.ndat, dtype = int)        
        # number of random pops to propose each year
        self.nRandInYr = np.zeros(self.nYears, dtype = int)  
        # make mask rasters for years 1910 - 11
        for i in range(2):
            datMask = self.yearsDefolDat == 1912
            col = self.col[datMask]
            row = self.row[datMask]
            self.rastMask[i, row, col] = 1
        # make mask rasters for years 1912-2003
        for i in range(self.nYears):
            datMask = self.yearsDefolDat == self.years[i]
            col = self.col[datMask]
            row = self.row[datMask]
            self.rastMask[(i + 2), row, col] = 1
            ntmp = len(col)                                 # len of data in year i
            dtmp = np.arange(ntmp)                          # data seq for year i
            self.datID[datMask] = dtmp        # append
            self.nRandInYr[i] = np.int(0.5 * ntmp)
        self.maskBool = self.rastMask == 1
        self.updateMask = self.maskBool[2:]

    def wrapNeighbourRastFX(self):
        """
        a wrapper fx for the numba function calcNeighboursErr
        """
        nNeighbour = np.ones_like(self.rastMask)
        # run numba fx
        self.nNeighbourRast = calcNeighboursErr(self.nYears, self.rastMask[2:], 
            self.errorDistArray, self.params.se_halfwinsize, self.rastShape,
            nNeighbour)
        
    def makeDefolWeath(self):
        """
        function to make 3-d arrays of defol data and  weather for 1912-2003
        """
        # 3-d arrays with zeros for defol data
        self.defol = np.zeros((self.nYears, rasterutils.N_YPIX, rasterutils.N_XPIX), dtype=np.uint8)
        defolDataAll = np.zeros(self.nTotDat, dtype = int)
        defolDataAll[self.defolAll > 0] = 1                          # np.ceil(self.defolAll)      
#        defolDataAll = defolDataAll.astype(np.integer)                               # make integer

        # 3-d arrays for weather with zeros to fill in
        minWinTemp = np.zeros((self.nYears, rasterutils.N_YPIX, rasterutils.N_XPIX))
        minSumTemp = np.zeros((self.nYears, rasterutils.N_YPIX, rasterutils.N_XPIX))
        pcpJune = np.zeros((self.nYears, rasterutils.N_YPIX, rasterutils.N_XPIX))
        # 1-d array with weather data
        minWT = self.defolWeath['minwT']                   
        minWT = minWT[self.maskRmEarlyMinn]
        minST = self.defolWeath['minsT']
        minST = minST[self.maskRmEarlyMinn]
        pcpJ = self.defolWeath['pcpJ']
        pcpJ = pcpJ[self.maskRmEarlyMinn]
        # scale weather covariates
        sc_minWT = (minWT - np.mean(minWT)) / np.std(minWT)
        sc_minST = (minST - np.mean(minST)) / np.std(minST)
        sc_pcpJ = (pcpJ - np.mean(pcpJ)) / np.std(pcpJ)
        # loop to populate defol and weath rasters
        for i in range(self.nYears):
            yearMask = self.yearsDefolDat == self.years[i]
            defolYear = defolDataAll[yearMask]
            col = self.col[yearMask]
            row = self.row[yearMask]
            self.defol[i, row, col] = defolYear            # defoliation rasters
            # get data for year i
            minwtyear = sc_minWT[yearMask]
            minstyear = sc_minST[yearMask]
            pcpjyear = sc_pcpJ[yearMask]
            # populate weather rasters
            minWinTemp[i, row, col] = minwtyear       # weather rasters
            minSumTemp[i, row, col] = minstyear
            pcpJune[i, row, col] = pcpjyear
        # a 4-d raster of weather 
        WeatherRast = np.array([minWinTemp, minSumTemp, pcpJune])
        self.weatherVariables = WeatherRast[self.params.WeatherIndx]

    def makePopRast(self):
        """
        function to make 3-d array of latent population size from 1910 - 2003
        Note that population is an unobserved latent variable that we are estimating
        These are initial values, in this case they are random.
        """
        # pop 3-d raster with zeros to be filled in
        self.popRast = np.zeros(((self.nYears + 2), rasterutils.N_YPIX, rasterutils.N_XPIX))

        # temp rasters to set initial population values for defoliated cells
        lowDefolRast = np.empty((rasterutils.N_YPIX, rasterutils.N_XPIX))
        lowDefolRast.fill(-1.)
        highDefolRast = np.empty((rasterutils.N_YPIX, rasterutils.N_XPIX))
        highDefolRast.fill(3.)

        # temp rasters to set initial population values for non-defoliated cells
        lowNonDefolRast = np.empty((rasterutils.N_YPIX, rasterutils.N_XPIX))
        lowNonDefolRast.fill(-3.)
        highNonDefolRast = np.empty((rasterutils.N_YPIX, rasterutils.N_XPIX))
        highNonDefolRast.fill(1.)

        # loop to populate the population rasters for 1910-1911
        for i in range(2):
            maskDefol = self.defol[0] == 1
            low = np.where(maskDefol, lowDefolRast, lowNonDefolRast)
            high = np.where(maskDefol, highDefolRast, highNonDefolRast)
            self.popRast[i] = np.random.uniform(low=low, high=high)

        # loop to populate the population rasters 1912-2003
        for i in range(self.nYears):
            maskDefol = self.defol[i] == 1
            low = np.where(maskDefol, lowDefolRast, lowNonDefolRast)
            high = np.where(maskDefol, highDefolRast, highNonDefolRast)
            self.popRast[i+2] = np.random.uniform(low=low, high=high)

        # multiple times yearly mask to put zeros where masked out
        self.popRast = self.popRast * self.rastMask  
        self.expPopRast = np.exp(self.popRast) * self.rastMask
        self.probObs = inv_logit(self.popRast[2:])         


    def proposeNewPop(self):
        """
        Function to propose new latent population estimates in 3-d array
        """
        # propose new variate from normal distribution with sd "searchPopVar"
        # loop to propose the population rasters for 1910-1911
        self.popRast_s = self.popRast.copy()
        for i in range(2):
            datMask = self.yearsDefolDat == 1912
            col = self.col[datMask]
            row = self.row[datMask]
            dID = self.datID[datMask]                                       # seq data for yr
            nrand = self.nRandInYr[0]                                       # number to propose
            rid = np.random.choice(dID, nrand, replace = False)             # cell to propose
            randV = np.random.normal(0.0, 0.075, nrand)   # .07   self.params.searchPopVar, nrand)    # random variates
            col2 = col[rid]                                                 # col and row id to propose
            row2 = row[rid]
            self.popRast_s[i, row2, col2] = self.popRast[i, row2, col2] + randV # add random variate
        # loop through 1912 - 2003
        for i in self.seqI:
            datMask = self.yearsDefolDat == self.years[i-2]
            col = self.col[datMask]
            row = self.row[datMask]
            dID = self.datID[datMask]                                       # seq data for yr
            nrand = self.nRandInYr[i-2]                                            # number to propose
            rid = np.random.choice(dID, nrand, replace = False)             # cell to propose
            randV = np.random.normal(0.0, 0.075, nrand)      # self.params.searchPopVar, nrand)    # random variates
#            if i == 34:
#                print('i', i, 'nrand', nrand, 'totpoints', len(col))
            col2 = col[rid]                                                 # col and row id to propose
            row2 = row[rid]
            self.popRast_s[i, row2, col2] = self.popRast[i, row2, col2] + randV # add random varia
        # multiply times mask to put zeros outside of area of interest
        self.popRast_s = self.popRast_s * self.rastMask
        expPop = np.exp(self.popRast_s) 
        self.expPopRast_s = expPop * self.rastMask
        # delete mask from memory
#        del self.maskRmEarlyMinn

    def spatialEffectFX(self, distarray):
        """
        make spatial effects 3-d array for years 1912 - 2003
        See model doc to see that neighbours at t-1 influence a cell at t
        """
        # empty 3-d spatial effects raster to be filled in.
        spatialEffect = np.zeros((self.nYears, rasterutils.N_YPIX, rasterutils.N_XPIX))

        for i in range(self.nYears):
            # get spatial effect for year i from population in year i-1
            # note that there are popRast for 1910 on and spatialEff rast 1912 on.    
            spatialEffect[i] = rasterutils.calculateSpatialEffect(self.expPopRast[i+1], 
                self.rastMask[i+2], distarray, self.params.se_halfwinsize, self.logPlus)
#            spatialEffect[i] = np.log(spatialEffect[i] + self.logPlus)
        return spatialEffect

    def makeClimateRast(self):
        """
        Make 3-d arrays for the two climate PCA variables (ax1 and ax2)
        Done for years 1912 - 2003.
        No updating
        """
        # create temp mask to get raster rows and col to populate rasters below
        datMask = self.yearsDefolDat == 1960       # post 1954 template include ONT and MN
        col = self.col[datMask]
        row = self.row[datMask]

        # make ax1 climate 3-d arrays with zeros
        ax1Rast = np.zeros((self.nYears, rasterutils.N_YPIX, rasterutils.N_XPIX))
        # scale the ax1 to mean = 0 and sd = 1
        scaleAX1 = ((self.climate['ax1'] - np.mean(self.climate['ax1'])) / 
            np.std(self.climate['ax1']))
        ax1Rast[..., row, col] = scaleAX1
        ax1Rast = ax1Rast * self.rastMask[2:]

        # 3-d array for climate ax2
        ax2Rast = np.zeros((self.nYears, rasterutils.N_YPIX, rasterutils.N_XPIX))
        # scale ax2 mean = 0 sd = 1
        scaleAX2 = ((self.climate['ax2'] - np.mean(self.climate['ax2'])) / 
            np.std(self.climate['ax2']))
        ax2Rast[..., row, col] = scaleAX2
        ax2Rast = ax2Rast * self.rastMask[2:]        

        # 3-d array for percent cover Spruce and fir
        SpFirRast = np.zeros((self.nYears, rasterutils.N_YPIX, rasterutils.N_XPIX))
        # scale SpFir mean = 0 sd = 1
        pct_SpFir_Full = self.landCovDat['PCT_SPRFIR_FULL']
        scaleSpFir = ((pct_SpFir_Full - np.mean(pct_SpFir_Full)) / 
            np.std(pct_SpFir_Full))
        SpFirRast[..., row, col] = scaleSpFir
        SpFirRast = SpFirRast * self.rastMask[2:]        

        # make a 4-d array for potential use in making beta rasters
        # model variables are indexed in params object
        climateRast = np.array([ax1Rast, ax2Rast, SpFirRast])                         
        b0_climateVariables = climateRast[self.params.b0_climateIndx]
        b1_climateVariables = climateRast[self.params.b1_climateIndx]
        b2_climateVariables = climateRast[self.params.b2_climateIndx]
        return (b0_climateVariables, b1_climateVariables, b2_climateVariables)

    def makeBetaRasters(self, alpha, climateVariables):
        """
        make 3-d rasters for B0, B1, and B2
        Betas in equation 5
        Use to update alpha parameters.
        """
        # Cycle thru climate 3-d arrays for B0 to get B0_raster
        Beta_rast = np.zeros((self.nYears, rasterutils.N_YPIX, rasterutils.N_XPIX))
        for i in range(len(alpha) - 1):
            np.add(Beta_rast, (alpha[i + 1] * climateVariables[i]), out = Beta_rast)
#            Beta_rast += alpha[i + 1] * climateVariables[i]
        # Add intercept alpha value
        np.add(Beta_rast, alpha[0], out = Beta_rast)
        return (Beta_rast)

    def calcDensityDepend(self, B1, popRast):
        """
        Calculate the density dependence effect
        Use when update alpha for Beta1, or update population
        """
        DD = B1 * popRast
        return DD

    def calcDelayedDensityDepend(self, B2, popRast):
        """
        Calculate the DELAYED density dependence effect
        Use when update alpha for Beta2, or update population
        """
        DDD = B2 * popRast
        return DDD


    def calcWeatherGamma(self):
        """
        multiply gamma times weather variable and keep as 3 or 4-d array
        """
        self.weatherGamma = self.weatherVariables.copy()
        for i in range(len(self.gamma)):
            self.weatherGamma[i] = self.gamma[i] * self.weatherVariables[i]
        

    def calcWeatherEffect(self, weatherGamma):
        """
        Calc 3-d weather effect using variables and gamma parameters
        """
        return np.sum(weatherGamma, axis = 0)


    def PopPredict(self, B0, DD, DDD, SE, WE):
        """
        calc population prediction for all years and cells
        This is prediction of population (latent variable) using parameters
        Equation 5
        """
        tmpPopPred = B0 + DD + DDD + SE + WE
        return(tmpPopPred)

    def makeInitialTauArray(self):
        """
        make prob detection raster and detection error raster
        """
        self.errByYear = np.random.uniform(0.0, 1.0, (self.nYears, 1, 1)) # 3-d array or errors by year
        self.errByYear = np.zeros((self.nYears, 1,1))
#        self.errorDetRast = np.zeros_like(self.popPred)        # detection error raster
        self.tauByYear = np.exp(self.delta0 + self.errByYear)
#        self.tauRast = np.zeros_like(self.popPred)       # probability of detection raster
#        self.tauRast = np.exp(self.delta0 + self.tauRast)
#        self.probObs = 1 - np.exp(-(self.expPopRast[2:] * self.tauRast))   # probability of getting observed data
        self.probObs = 1 - np.exp(-(self.expPopRast[2:] * self.tauByYear))   # probability of getting observed data

    def calcDNormSD(self, sd0):
        """
        to be used in dnormPDF
        """
        var0 = sd0 * sd0
        var = var0 / self.nNeighbourRast
        sd = np.sqrt(var)
        precalc = 1.0/sd/(np.sqrt(2.0*np.pi))
        denomcalc = 2.0*var
        return (precalc, denomcalc)

class MCMC(object):
    def __init__(self, params, basicdata):
        self.basicdata = basicdata
        self.params = params
        # storage arrays for the alpha_B0, B1 and B1 parameter for climate effects  
        self.B0gibbs = np.zeros([self.params.ngibbs, len(self.basicdata.alpha_b0)]) #rate of growth
        self.B1gibbs = np.zeros([self.params.ngibbs, len(self.basicdata.alpha_b1)]) # density dependence
        self.B2gibbs = np.zeros([self.params.ngibbs, len(self.basicdata.alpha_b2)]) # delayed density dependence
        self.kgibbs = np.zeros(self.params.ngibbs)          # spatial effect decay
        self.siggibbs = np.zeros(self.params.ngibbs)        # variance
#        self.psigibbs = np.zeros(self.params.ngibbs)        # psi spatial effect intensity
        self.gammagibbs = np.zeros([self.params.ngibbs, len(self.basicdata.gamma)]) # gamma weather
#        self.sigmaDetectgibbs = np.zeros(self.params.ngibbs) 
#        self.rhogibbs = np.zeros([self.params.ngibbs, self.basicdata.nYears]) 
#        self.rhogibbs = np.zeros(self.params.ngibbs) 
###        self.delta0gibbs = np.zeros(self.params.ngibbs) 
        self.popgibbs = np.zeros([self.params.ngibbs, 4]) 
###        self.errDetectgibbs = np.zeros([self.params.ngibbs, 4])
###        self.errByYeargibbs = np.zeros([self.params.ngibbs, self.basicdata.nYears])


    def vupdate(self):
        predDiff= self.basicdata.popRast[2:] - self.basicdata.popPred
        predDiff = predDiff * self.basicdata.rastMask[2:]
        sx = np.sum(predDiff*predDiff)
        u1 = self.params.s1 + np.multiply(.5, self.basicdata.ndat)
        u2 = self.params.s2 + np.multiply(.5, sx)
        isg = np.random.gamma(u1, 1.0/u2, size = None)
        self.basicdata.sigma = 1.0/isg
        self.basicdata.sqrt_sigma = np.sqrt(self.basicdata.sigma)

    def dnorm1SD(self, arr1, mn, sd):
        precalc = 1.0/sd/(np.sqrt(2.0*np.pi))
        denomcalc = 2.0*sd*sd
        diff = (arr1 - mn)
        numcalc = diff*diff
        normpdf = precalc * np.exp(-numcalc/denomcalc)
        return normpdf

#    def dbern(self,d, p):
#        pp = np.where(d==1, p, 1.0-p)
#        return pp

    def dbern2(self,d, p):
        pp = (p**d) * ((1-p)**(1-d))
#        np.where(pp == 0.0, 1.0e-5, pp)
        return pp


    def popUpdate1910(self):
        """
        Update latent population for 1910
        """
        # Relevant pop rasters
        popRast1912 = self.basicdata.popRast[2]                 # 1912 - target of prediction using proposed pop in 1910
        popRast1910 = self.basicdata.popRast[0]                 # 1910 current value
        popRast1910_s = self.basicdata.popRast_s[0]             # 1910 proposed value
        # population prediction in 1912 using current parameters
        popPred1912 = self.basicdata.popPred[0]  
        # calc new prediction in 1912 using proposed new pop in 1910 (vary DDD only)
        DDD = self.basicdata.delayedDensityDepend[0]                            
        B2 = self.basicdata.B2_rast[0]              
        DDD_s = self.basicdata.calcDelayedDensityDepend(B2, popRast1910_s)
        popPred1912_s = popPred1912 - DDD + DDD_s
        # calc importance ratio of current and proposed pop values in 1910
        norm_pdf = self.dnorm1SD(popRast1912, popPred1912, 
                            self.basicdata.sqrt_sigma)    # probability density
        norm_pdf_s = self.dnorm1SD(popRast1912, popPred1912_s, 
                               self.basicdata.sqrt_sigma)

        pnow = (np.log(norm_pdf) + 
                np.log(self.dnorm1SD(popRast1910, self.params.popPriorMean, 
                        self.params.popPriorSD)))
        pnew = (np.log(norm_pdf_s) + 
                np.log(self.dnorm1SD(popRast1910_s, self.params.popPriorMean, 
                        self.params.popPriorSD)))
        rValue = np.exp(pnew - pnow)        # calc importance ratio (IR)
        # random probability to accept
        zValue = np.random.uniform(self.basicdata.zerosRaster, 
                        self.basicdata.rastMask[0])
        rvMask = rValue > zValue

        # if IR greater that zValue update
        self.basicdata.popRast[0, rvMask] = popRast1910_s[rvMask]
        self.basicdata.delayedDensityDepend[0, rvMask] = DDD_s[rvMask]
        self.basicdata.popPred[0, rvMask] = popPred1912_s[rvMask] 

    def popUpdate1911(self):
        """
        Update latent population for 1911, which influences 1912 and 1913
        """
        # Relevant pop rasters
        popRast1913 = self.basicdata.popRast[3] 
        popRast1912 = self.basicdata.popRast[2]                 # 1912 - target of prediction using proposed pop in 1911
        popRast1911 = self.basicdata.popRast[1]                 # 1911 current value
        popRast1911_s = self.basicdata.popRast_s[1]             # 1911 proposed value
        expPopRast1911_s = self.basicdata.expPopRast_s[1]
        # population prediction in 1912 using current parameters
        popPred1912 = self.basicdata.popPred[0]
        popPred1913 = self.basicdata.popPred[1]
        # calc DD and DDD for 1912 and 1913
        B2_13 = self.basicdata.B2_rast[1]
        B1_12 = self.basicdata.B1_rast[0]              
        DD_s_12 = self.basicdata.calcDensityDepend(B1_12, popRast1911_s)            #DD - 1912
        DD_12 = self.basicdata.densityDepend[0]                                     #DD - 1913
        DDD_13 = self.basicdata.delayedDensityDepend[1]                             #DDD - 1912
        DDD_s_13 = self.basicdata.calcDelayedDensityDepend(B2_13, popRast1911_s)    #DDD - 1913
        # spatial effects for 1912
        SE12_s = rasterutils.calculateSpatialEffect(expPopRast1911_s,
                self.basicdata.rastMask[2], self.basicdata.se_distarray,
                self.params.se_halfwinsize, self.basicdata.logPlus)
#        SE12_s = np.log(SE12_s + self.basicdata.logPlus)                                 #* self.basicdata.psi
        SE12 = self.basicdata.spatialEffectRast[0]
        # Predict population size in 1912-13 given proposed pop in 1911
        popPred1912_s = popPred1912 - DD_12 + DD_s_12 - SE12 + SE12_s 
        popPred1913_s = popPred1913 - DDD_13 + DDD_s_13
        # do analysis over masked pixels
        pixMask = self.basicdata.maskBool[1]
        # calc importance ratio of current and proposed pop values in 1911

        normNow = ((np.log(self.dnorm1SD(popRast1912,
                        popPred1912, self.basicdata.sqrt_sigma))) +
                   (np.log(self.dnorm1SD(popRast1913, popPred1913,
                        self.basicdata.sqrt_sigma))))
        priorNow = np.log(self.dnorm1SD(popRast1911, self.params.popPriorMean,
                        self.params.popPriorSD))
        pnowSum = (np.sum(normNow[pixMask]) + np.sum(priorNow[pixMask]))
                
        # pdf data in 1912-13 given proposed pop in 1911
        normNew = ((np.log(self.dnorm1SD(popRast1912,
                        popPred1912_s, self.basicdata.sqrt_sigma))) +
                    (np.log(self.dnorm1SD(popRast1913, popPred1913_s,
                        self.basicdata.sqrt_sigma))))
        priorNew = np.log(self.dnorm1SD(popRast1911_s, self.params.popPriorMean,
                        self.params.popPriorSD))
        pnewSum = (np.sum(normNew[pixMask]) +  np.sum(priorNow[pixMask]))

#        print('pnewSum-pnowSum', pnewSum-pnowSum)
#        print('pnowSum', pnowSum)

        pdiff = pnewSum - pnowSum
        if pdiff > 2:                       # need this because initial parameter are way off
            rValue = 1.0                      # without this step, get LogLik = inf or -inf
            zValue = 0.0
        else:
            rValue = np.exp(pdiff)        # calc importance ratio
            zValue = np.random.uniform(0, 1.0, size = None)

        # calc importance ratio (IR)
#        rValue = np.exp(pnewSum - pnowSum)        # calc importance ratio (IR)
        # random probability to accept
#        zValue = np.random.uniform(0.0, 1.0, size = None)
        # if IR greater that zValue update pop, DD, DDD and Spatial effect
        if rValue > zValue:
            self.basicdata.popRast[1] = popRast1911_s
            self.basicdata.expPopRast[1] = expPopRast1911_s
            self.basicdata.densityDepend[0] = DD_s_12
            self.basicdata.popPred[0] = popPred1912_s
            self.basicdata.delayedDensityDepend[1] = DDD_s_13
            self.basicdata.spatialEffectRast[0] = SE12_s
            self.basicdata.popPred[1] = popPred1913_s

    def popUpdate1912(self):
        """
        update population from 1912 to third to last year (2010)
        """
        # cycle thru years. seqI is arange(2, nyears)
        for i in self.basicdata.seqI:
            # defoliation raster in Time t
            defol_T = self.basicdata.defol[i-2]
            # Relevant pop rasters
            popRast_T = self.basicdata.popRast[i]                  # pop in year t
            popRast_T_s = self.basicdata.popRast_s[i].copy()              # proposed pop at t
            expPopRast_T_s = self.basicdata.expPopRast_s[i].copy()   #################### 
            popRast_T_Up1 = self.basicdata.popRast[i+1]
            popRast_T_Up2 = self.basicdata.popRast[i+2]
            # current population predictions
            popPred_T = self.basicdata.popPred[i-2]
            # population prediction in up 1 and 2 years using current parameters
            popPredUp1 = self.basicdata.popPred[i-1]
            popPredUp2 = self.basicdata.popPred[i]
            # calc DD and DDD for up one and two years
            B2_Up2 = self.basicdata.B2_rast[i]
            B1_Up1 = self.basicdata.B1_rast[i-1]              
            DD_s_Up1 = self.basicdata.calcDensityDepend(B1_Up1, popRast_T_s)            #DD
            DD_Up1 = self.basicdata.densityDepend[i-1]                                     #DD - 1913
            DDD_Up2 = self.basicdata.delayedDensityDepend[i]                             #DDD - 1912
            DDD_s_Up2 = self.basicdata.calcDelayedDensityDepend(B2_Up2, popRast_T_s)    #DDD - 1913
            # spatial effects for up one year
            SE_s_Up1 = rasterutils.calculateSpatialEffect(expPopRast_T_s,
                    self.basicdata.rastMask[i+1], self.basicdata.se_distarray,
                    self.params.se_halfwinsize, self.basicdata.logPlus)
#            SE_s_Up1 = np.log(SE_s_Up1 + self.basicdata.logPlus)                                 #* self.basicdata.psi
            SE_Up1 = self.basicdata.spatialEffectRast[i-1]
            # Predict population size up 1 and 2 years for new pop values
            popPred_s_Up1 = popPredUp1 - DD_Up1 + DD_s_Up1 - SE_Up1 + SE_s_Up1             
            popPred_s_Up2 = popPredUp2 - DDD_Up2 + DDD_s_Up2 
            # calc normal pdf for T and up 1 and up 2 years
            norm_pdfT = np.log(self.dnorm1SD(popRast_T, 
                                popPred_T, self.basicdata.sqrt_sigma))
            norm_pdfT_s = np.log(self.dnorm1SD(popRast_T_s, 
                                popPred_T, self.basicdata.sqrt_sigma))

            norm_pdfUp1 = np.log(self.dnorm1SD(popRast_T_Up1, 
                                popPredUp1, self.basicdata.sqrt_sigma))
            norm_pdfUp1_s = np.log(self.dnorm1SD(popRast_T_Up1, 
                                popPred_s_Up1, self.basicdata.sqrt_sigma))

            norm_pdfUp2 = np.log(self.dnorm1SD(popRast_T_Up2, 
                                popPredUp2, self.basicdata.sqrt_sigma))
            norm_pdfUp2_s = np.log(self.dnorm1SD(popRast_T_Up2, 
                                popPred_s_Up2, self.basicdata.sqrt_sigma))
            # calc bernouilli density
###            probObs = 1 - np.exp(-(self.basicdata.expPopRast[i] * self.basicdata.tauByYear[i - 2]))
#            probObs = inv_logit(popRast_T)
#            print('prob obs', np.min(probObs), np.max(probObs))
            maskT = self.basicdata.maskBool[i]
            dbernNow = stats.binom.pmf(defol_T, 1, self.basicdata.probObs[i - 2])      #np.log(self.dbern2(defol_T, probObs))
            dbernNow[maskT] = np.log(dbernNow[maskT])
            probObs_s = inv_logit(popRast_T_s) 
            dbernNew = stats.binom.pmf(defol_T, 1, probObs_s)  #np.log(self.dbern2(defol_T, probObs_s))
            dbernNew[maskT] = np.log(dbernNew[maskT])

            # likelihood
            pnow = mdl.makestack((norm_pdfT, norm_pdfUp1, norm_pdfUp2, dbernNow))
            pnew = mdl.makestack((norm_pdfT_s, norm_pdfUp1_s, norm_pdfUp2_s, dbernNew))
            # mask for current and next two years
            maskT12 = self.basicdata.maskBool[i : (i+3)]
            tmpMask = mdl.makestack([maskT12, maskT])
            # sum over masked areas
            tmpNow = np.ma.array(pnow, mask=np.logical_not(tmpMask))
            pnowSum = tmpNow.sum()
            tmpNew = np.ma.array(pnew, mask=np.logical_not(tmpMask))
            pnewSum = tmpNew.sum()
            # calc importance ratio (IR)
            pdiff = pnewSum - pnowSum
            if pdiff > 20.0:                       # need this because initial parameter are way off
                rValue = 1.0                      # without this step, get LogLik = inf or -inf
                zValue = 0.0
            elif pdiff < -20.0:
                rValue = 0.0                      # without this step, get LogLik = inf or -inf
                zValue = 1.0
            else:
                rValue = np.exp(pdiff)        # calc importance ratio
                zValue = np.random.uniform(0.0, 1.0, size = None)

            if rValue > zValue:
                self.basicdata.popRast[i] = popRast_T_s
                self.basicdata.expPopRast[i] = expPopRast_T_s
                #update DD and DDD using updated pop in time t
                self.basicdata.densityDepend[i-1] = DD_s_Up1
                self.basicdata.delayedDensityDepend[i] = DDD_s_Up2
                # update spatial effect one year ahead
                self.basicdata.spatialEffectRast[i-1] = SE_s_Up1
                # update popPred up 1 and 2
                self.basicdata.popPred[i-1] = popPred_s_Up1
                self.basicdata.popPred[i] = popPred_s_Up2
#                self.basicdata.defolProb[i - 2] = defolProb_s
                self.basicdata.probObs[i - 2] = probObs_s

    def popUpdateNextToLast(self):
        """
        update next to last population (2002)
        """
        # set i to be second to last year
#        i = self.basicdata.nYears
        # defoliation raster in Time t
        defol_T = self.basicdata.defol[-2]
        # Relevant pop rasters
        popRast_T = self.basicdata.popRast[-2]                  # pop in year t
        popRast_T_s = self.basicdata.popRast_s[-2]              # proposed pop at t
        expPopRast_T_s = self.basicdata.expPopRast_s[-2]
        popRast_T_Up1 = self.basicdata.popRast[-1]
        # current population predictions
        popPred_T = self.basicdata.popPred[-2]
        # population prediction in up 1 and 2 years using current parameters
        popPredUp1 = self.basicdata.popPred[-1]
        # calc DD and DDD for up one and two years
        B1_Up1 = self.basicdata.B1_rast[-1]    
        DD_s_Up1 = self.basicdata.calcDensityDepend(B1_Up1, popRast_T_s)            #DD
        DD_Up1 = self.basicdata.densityDepend[-1]                                     #DD - 1913
        # spatial effects for up one year
        SE_s_Up1 = rasterutils.calculateSpatialEffect(expPopRast_T_s,
                    self.basicdata.rastMask[-1] , self.basicdata.se_distarray,
                    self.params.se_halfwinsize, self.basicdata.logPlus)
#        SE_s_Up1 = np.log(SE_s_Up1 + self.basicdata.logPlus)                                 #* self.basicdata.psi
        SE_Up1 = self.basicdata.spatialEffectRast[-1]
        # Predict population size up 1 and 2 years for new pop values
        popPred_s_Up1 = popPredUp1 - DD_Up1 + DD_s_Up1 - SE_Up1 + SE_s_Up1             
        # calc normal pdf for T and up 1 and up 2 years
        norm_pdfT = np.log(self.dnorm1SD(popRast_T, 
                        popPred_T, self.basicdata.sqrt_sigma))
        norm_pdfT_s = np.log(self.dnorm1SD(popRast_T_s, 
                        popPred_T, self.basicdata.sqrt_sigma))
        norm_pdfUp1 = np.log(self.dnorm1SD(popRast_T_Up1, 
                        popPredUp1, self.basicdata.sqrt_sigma))
        norm_pdfUp1_s = np.log(self.dnorm1SD(popRast_T_Up1, 
                        popPred_s_Up1, self.basicdata.sqrt_sigma))
        # calc bernouilli density
        probObs = self.basicdata.probObs[-2]   # -5
        maskT = self.basicdata.maskBool[-2]
        dbernNow = stats.binom.pmf(defol_T, 1, probObs)      #np.log(self.dbern2(defol_T, probObs))
        dbernNow[maskT] = np.log(dbernNow[maskT])
        probObs_s = inv_logit(popRast_T_s) 
        dbernNew = stats.binom.pmf(defol_T, 1, probObs_s)  #np.log(self.dbern2(defol_T, probObs_s))
        dbernNew[maskT] = np.log(dbernNew[maskT])

        
        # likelihood
        pnow = mdl.makestack((norm_pdfT, norm_pdfUp1, dbernNow))
        pnew = mdl.makestack((norm_pdfT_s, norm_pdfUp1_s, dbernNew))
        # mask for current and next two years
        maskindx = np.array([92, 93, 92])    
        tmpMask = self.basicdata.maskBool[maskindx]
        # sum over masked areas
        tmpNow = np.ma.array(pnow, mask=np.logical_not(tmpMask))
        pnowSum = tmpNow.sum()
        tmpNew = np.ma.array(pnew, mask=np.logical_not(tmpMask))
        pnewSum = tmpNew.sum()
        # calc importance ratio (IR)
        pdiff = pnewSum - pnowSum
        if pdiff > 2:                       # need this because initial parameter are way off
            rValue = 1.0                      # without this step, get LogLik = inf or -inf
            zValue = 0.0
        elif pdiff < -100:
            rValue = 0.0                      # without this step, get LogLik = inf or -inf
            zValue = 1.0
        else:
            rValue = np.exp(pdiff)        # calc importance ratio
            zValue = np.random.uniform(0, 1.0, size = None)
        if rValue > zValue:
            self.basicdata.popRast[-2] = popRast_T_s
            self.basicdata.expPopRast[-2] = expPopRast_T_s
            #update DD and DDD using updated pop in time t
            self.basicdata.densityDepend[-1] = DD_s_Up1
            # update spatial effect one year ahead
            self.basicdata.spatialEffectRast[-1] = SE_s_Up1
            # update popPred up 1 
            self.basicdata.popPred[-1] = popPred_s_Up1
            self.basicdata.probObs[-2] = probObs_s

    def popUpdateLastYear(self):
        """
        update population of last year (2003)
        """
        popRast_T = self.basicdata.popRast[-1]       # last year pop
        popRast_T_s = self.basicdata.popRast_s[-1]   # last year proposed value
        # population prediction last year using current parameters
        popPred_T = self.basicdata.popPred[-1]
        # defoliation raster in Time t
        defol_T = self.basicdata.defol[-1]
        probObs = self.basicdata.probObs[-1]
#        defolProb_s = inv_logit(popRast_T_s)
        expPopRast_T_s = self.basicdata.expPopRast_s[-1]
        probObs_s = inv_logit(popRast_T_s)
        # calc bernouilli density
        probObs = self.basicdata.probObs[-1]    # -5
        maskT = self.basicdata.maskBool[-1]
        dbernNow = stats.binom.pmf(defol_T, 1, probObs)      #np.log(self.dbern2(defol_T, probObs))
        dbernNow[maskT] = np.log(dbernNow[maskT])
        probObs_s = inv_logit(popRast_T_s) 
        dbernNew = stats.binom.pmf(defol_T, 1, probObs_s)  #np.log(self.dbern2(defol_T, probObs_s))
        dbernNew[maskT] = np.log(dbernNew[maskT])

        # calc importance ratio of current and proposed pop values in 1911
        norm_pdf = np.log(self.dnorm1SD(popRast_T, popPred_T, self.basicdata.sqrt_sigma))
        norm_pdf_s = np.log(self.dnorm1SD(popRast_T_s, popPred_T, self.basicdata.sqrt_sigma)) 

        pnow = norm_pdf + dbernNow                             # add logit
        pnew = norm_pdf_s + dbernNew                         # add logit
        # calc importance ratio (IR)
        rValue = np.exp(pnew - pnow)        # calc importance ratio (IR)
        # random probability to accept
        zValue = np.random.uniform(self.basicdata.zerosRaster, self.basicdata.rastMask[-1])
        # if IR greater that zValue update pop, DD, DDD and Spatial effect
        rvMask = rValue > zValue
        self.basicdata.popRast[-1, rvMask] = popRast_T_s[rvMask]
#        self.basicdata.defolProb[-1, rvMask] = defolProb_s[rvMask]
        self.basicdata.probObs[-1, rvMask] = probObs_s[rvMask]
        self.basicdata.expPopRast[-1, rvMask] = expPopRast_T_s[rvMask]

    def popUpdateAll(self):
        """
        Update latent population for all years
        Call this function in mcmcfx
        """
        self.popUpdate1910()
        self.popUpdate1911()
        self.popUpdate1912()
        self.popUpdateNextToLast()
        self.popUpdateLastYear()
        self.basicdata.proposeNewPop()

    def errSumZero(self):
        self.errByYear_s = self.basicdata.errByYear * 1.0        # .copy()
        self.errByYear_s[self.yearsChange] = np.random.normal(self.basicdata.errByYear[self.yearsChange], 
                .05)
#        self.errByYear_s[i] = np.random.normal(self.basicdata.errByYear[i], .01, size=None)
        self.errByYear_s = self.errByYear_s - np.mean(self.errByYear_s)


    def updateDetectError(self):
        """
        # update latent error in detection probability
        """
        self.yearsChange = np.random.choice(self.basicdata.seqYears, 
            self.params.nYearsChange, replace = False)
        self.errSumZero()
        tauByYear_s = np.exp(self.basicdata.delta0 + self.errByYear_s)
        probObs_s = 1.0 - np.exp(-(self.basicdata.expPopRast[2:] * tauByYear_s))   # proposed probability of getting observed data
        dbernNow = np.log(self.dbern2(self.basicdata.defol, self.basicdata.probObs))
        sumdbernNow = np.sum(dbernNow * self.basicdata.rastMask[2:])
        dbernNew = np.log(self.dbern2(self.basicdata.defol, probObs_s))
        sumdbernNew = np.sum(dbernNew * self.basicdata.rastMask[2:])
        priorNow = np.sum(stats.norm.logpdf(self.basicdata.errByYear, 0.0, 2.0))
        priorNew = np.sum(stats.norm.logpdf(self.errByYear_s, 0.0, 2.0))
        pnow = sumdbernNow + priorNow
        pnew = sumdbernNew + priorNew
        pdiff = pnew - pnow
        if pdiff > 5.0:                       # need this because initial parameter are way off
            rValue = 1.0                      # without this step, get LogLik = inf or -inf
            zValue = 0.0
        elif pdiff < -5.0:
            rValue = 0.0                      # without this step, get LogLik = inf or -inf
            zValue = 1.0
        else:
            rValue = np.exp(pdiff)        # calc importance ratio
            zValue = np.random.uniform(0, 1.0, size = None)
#            rValue = np.exp(pnew - pnow)
#            zValue = np.random.random()
        if rValue > zValue:
            self.basicdata.errByYear = self.errByYear_s.copy()
            self.basicdata.probObs = probObs_s.copy()
            self.basicdata.tauByYear = tauByYear_s.copy()


    def updateSigmaDetect(self):
        """
        # update detection-error variance parameter
        """
        self.sigmaDetect_s = np.exp(np.random.normal(np.log(self.basicdata.sigmaDetect), .015, size = None))
        (precalc_s, denomcalc_s) = self.basicdata.calcDNormSD(self.sigmaDetect_s)        

        (self.basicdata.mu_errDetectRast, self.NowDNorm, self.NewDNorm) = muError(self.basicdata.nYears,
              self.basicdata.errorDetRast, self.basicdata.rastMask[2:], self.basicdata.errorDistArray, 
              self.params.se_halfwinsize, self.basicdata.precalc, self.basicdata.denomcalc, 
              precalc_s, denomcalc_s, self.basicdata.mu_errDetectRast, self.basicdata.rastShape)

#        print('self.NowDNorm, self.NewDNorm', self.NowDNorm, self.NewDNorm)

        priorNow = gamma_pdf(self.basicdata.sigmaDetect*self.basicdata.sigmaDetect, 
                            self.params.priorSigmaDetect[0], 1/self.params.priorSigmaDetect[1])
        priorNew = gamma_pdf(self.sigmaDetect_s * self.sigmaDetect_s, 
                            self.params.priorSigmaDetect[0], 1/self.params.priorSigmaDetect[1])
#        print('sig_s, and sig', self.sigmaDetect_s, self.basicdata.sigmaDetect)
#        print('self.NowDNorm + priorNow', self.NowDNorm, priorNow)
#        print('self.NewDNorm + priorNew', self.NewDNorm, priorNew)
        pnow = self.NowDNorm + priorNow
        pnew = self.NewDNorm + priorNew
        pdiff = pnew - pnow
        if pdiff > 2:                       # need this because initial parameter are way off
            rValue = 1.0                      # without this step, get LogLik = inf or -inf
            zValue = 0.0
        elif pdiff < -100:
            rValue = 0.0                      # without this step, get LogLik = inf or -inf
            zValue = 1.0
        else:
            rValue = np.exp(pdiff)        # calc importance ratio
            zValue = np.random.uniform(0, 1.0, size = None)
#        print('r and z', rValue, zValue)
        # if proposed parameter is better, keep it and update
        if rValue > zValue:
            self.basicdata.sigmaDetect = self.sigmaDetect_s
            self.NowDNorm = self.NewDNorm
            self.basicdata.precalc = precalc_s.copy() 
            self.basicdata.denomcalc = denomcalc_s.copy()

    def updateRho(self):
        """
        # update rho praameter for mu error
        """
        rho_s = np.exp(np.random.normal(np.log(self.basicdata.rho), 0.1, size = None))
#        rho_s = np.exp(np.random.normal(np.log(self.basicdata.rho), 0.1, size = self.basicdata.nYears))
        # small array for getting neighbourhood mu
        errorDistArray_s = errorDist(self.params.se_winsize, self.params.se_halfwinsize, 
                rho_s, self.basicdata.errdistarrayZero)
        priorNow = np.log(stats.norm.pdf(np.log(self.basicdata.rho), self.params.rhoPriors[0],
                    self.params.rhoPriors[1]))
        priorNew = np.log(stats.norm.pdf(np.log(rho_s), self.params.rhoPriors[0],
                    self.params.rhoPriors[1]))
        # empty error detect 3-d raster (e_it)
        mu_errDetectRast_s = np.empty((self.basicdata.nYears, self.basicdata.rastShape[0],
                self.basicdata.rastShape[1]))

        (mu_errDetectRast_s, NowDNorm, NewDNorm) =  rho_MuError(self.basicdata.rho, 
                rho_s, self.basicdata.nYears, self.basicdata.errorDetRast, 
                self.basicdata.rastMask, self.basicdata.errorDistArray, 
                errorDistArray_s , self.params.se_halfwinsize, self.basicdata.precalc, 
                self.basicdata.denomcalc, self.basicdata.mu_errDetectRast, 
                priorNow, priorNew, self.basicdata.rastShape, mu_errDetectRast_s)
        # if proposed parameter is better, keep it and update
        pnow = NowDNorm + priorNow
        pnew = NewDNorm + priorNew
        rValue = np.exp(pnew - pnow)
        zValue = np.random.random()
        if rValue > zValue:
            self.basicdata.rho = rho_s               
            self.basicdata.mu_errDetectRast = mu_errDetectRast_s.copy()
#            distarray = distarray_s.copy()




    def updateDelta0(self):
        """
        # update delta0 parameter
        """
        d0_s = np.random.normal(self.basicdata.delta0, 0.07, size = None)  # .06
        tauByYear_s = np.exp(d0_s + self.basicdata.errByYear)
        probObs_s = 1 - np.exp(-(self.basicdata.expPopRast[2:] * tauByYear_s))   # proposed probability of getting observed data

        dbernNow = np.log(self.dbern2(self.basicdata.defol, self.basicdata.probObs))
        dbernNew = np.log(self.dbern2(self.basicdata.defol, probObs_s))

        tmp = np.ma.array(dbernNow, mask=np.logical_not(self.basicdata.updateMask))
        NowDBernSum = tmp.sum()
        tmp = np.ma.array(dbernNew, mask=np.logical_not(self.basicdata.updateMask))
        NewDBernSum = tmp.sum()

        priorNow = np.log(stats.norm.pdf(self.basicdata.delta0, self.params.delta0Priors[0],
                    self.params.delta0Priors[1]))
        priorNew = np.log(stats.norm.pdf(d0_s, self.params.delta0Priors[0],
                    self.params.delta0Priors[1]))
        pnow = NowDBernSum + priorNow
        pnew = NewDBernSum + priorNew
        pdiff = pnew - pnow
        if pdiff > 2:                       # need this because initial parameter are way off
            rValue = 1.0                      # without this step, get LogLik = inf or -inf
            zValue = 0.0
        elif pdiff < -100:
            rValue = 0.0                      # without this step, get LogLik = inf or -inf
            zValue = 1.0
        else:
            rValue = np.exp(pdiff)        # calc importance ratio
            zValue = np.random.uniform(0, 1.0, size = None)
        # if proposed parameter is better, keep it and update B0 and popPred
        if rValue > zValue:
            self.basicdata.delta0 = d0_s
            self.basicdata.tauByYear = tauByYear_s.copy()
            self.basicdata.probObs = probObs_s.copy()
        

    def alphaB0_Update(self):
        """
        Update alpha parameters for calculating B0 (growth rate)
        """
        ## loop through alpha B0 parameters
        for i in range(len(self.basicdata.alpha_b0)):
            alpha_b0_s = self.basicdata.alpha_b0.copy()
            # draw new proposal from normal
            alpha_b0_s[i] = np.random.normal(self.basicdata.alpha_b0[i], 0.02, size = None)    #.01
            # proposed new B0 raster given proposed alpha parameter
            B0_rast_s = self.basicdata.makeBetaRasters(alpha_b0_s, 
                            self.basicdata.b0_climateVariables)
            popPred_s = (self.basicdata.PopPredict(B0_rast_s,
                           self.basicdata.densityDepend,
                            self.basicdata.delayedDensityDepend,
                            self.basicdata.spatialEffectRast,
                            self.basicdata.weatherEffect))            
            # calc IR
            # likelihood of population size given predictions times prior on parameters
            dnormPopPred = (np.log(self.dnorm1SD(self.basicdata.popRast[2:],
                            self.basicdata.popPred, self.basicdata.sqrt_sigma)))
            tmp = np.ma.array(dnormPopPred, mask=np.logical_not(self.basicdata.updateMask))
            dnormPopPredSum = tmp.sum()
            if i == 0:
                priorSD = self.params.alphaGrowthSD
            else:
                priorSD = self.params.alphaSD

            priorNow = (np.log(stats.norm.pdf(self.basicdata.alpha_b0[i],
                            self.params.alphaMean, priorSD)))
            pnowSum = dnormPopPredSum + priorNow

            dnormPopPred = (np.log(self.dnorm1SD(self.basicdata.popRast[2:],
                            popPred_s, self.basicdata.sqrt_sigma)))
            tmp = np.ma.array(dnormPopPred, mask=np.logical_not(self.basicdata.updateMask))
            dnormPopPredSum = tmp.sum()

            priorNew = (np.log(stats.norm.pdf(alpha_b0_s[i],
                            self.params.alphaMean, priorSD)))
            pnewSum = dnormPopPredSum + priorNew    

            pdiff = pnewSum - pnowSum
            if pdiff > 2:                       # need this because initial parameter are way off
                rValue = 1.0                      # without this step, get LogLik = inf or -inf
                zValue = 0.0
            elif pdiff < -100:
                rValue = 0.0                      # without this step, get LogLik = inf or -inf
                zValue = 1.0
            else:
                rValue = np.exp(pdiff)        # calc importance ratio
                zValue = np.random.uniform(0, 1.0, size = None)
            # if proposed parameter is better, keep it and update B0 and popPred
            if rValue > zValue:
                self.basicdata.B0_rast = B0_rast_s.copy()
                self.basicdata.alpha_b0[i] = alpha_b0_s[i]
                self.basicdata.popPred = popPred_s.copy()

#    def pdfwrap(self,arr1, mn, sd):
#        return self.dnorm1SD(arr1, mn, sd)


    def alphaBeta12_Update(self, alphaPara, bRaster, climateRast, P):
        """
        Update alpha parameters for calculating Beta 3-d rasters
        P is indicator for B1 (DD) or B2 (DDD)
        """
        ## loop through alpha Beta parameters
        for i in range(len(alphaPara)):
            alphaPara_s = alphaPara.copy()
            if P == 1:
                # draw new proposal from normal
                alphaPara_s[i] = np.random.normal(alphaPara[i], 0.01, size = None) #.005
#                alphaPara_s[i] = alphaPara[i]
                # proposed DD effect with new alpha
                # proposed new B0 raster given proposed alpha parameter
                bRaster_s = self.basicdata.makeBetaRasters(alphaPara_s, 
                            climateRast)
                DDEffect_s = (self.basicdata.calcDensityDepend(bRaster_s, 
                                self.basicdata.popRast[1:-1]))
                # predict pop with new alphas
                popPred_s = (self.basicdata.PopPredict(self.basicdata.B0_rast,
                            DDEffect_s,
                            self.basicdata.delayedDensityDepend,
                            self.basicdata.spatialEffectRast,
                            self.basicdata.weatherEffect))
            # population prediction if updating alphas for B2
            if P == 2:
                # draw new proposal from normal
                alphaPara_s[i] = np.random.normal(alphaPara[i], 0.005, size = None) #.005

#                alphaPara_s[i] = alphaPara[i]

                # proposed new B0 raster given proposed alpha parameter
                bRaster_s = self.basicdata.makeBetaRasters(alphaPara_s, 
                            climateRast)
                DDDEffect_s = (self.basicdata.calcDelayedDensityDepend(bRaster_s, 
                             self.basicdata.popRast[0:-2]))
                
#                print('DDD now',self.basicdata.delayedDensityDepend[32,20,50])
#                print('DDD new', DDDEffect_s[32,20,50])
#                print('#######################################################')
                

                popPred_s = (self.basicdata.PopPredict(self.basicdata.B0_rast,
                            self.basicdata.densityDepend,
                            DDDEffect_s,
                            self.basicdata.spatialEffectRast,
                            self.basicdata.weatherEffect))

#            rastPP = (self.basicdata.PopPredict(self.basicdata.B0_rast,
#                            self.basicdata.densityDepend,
#                            self.basicdata.delayedDensityDepend,
#                            self.basicdata.spatialEffectRast,
#                            self.basicdata.weatherEffect))



#            for ii in range(self.basicdata.nYears):
#                ppi_s = np.sum(popPred_s[ii] * self.basicdata.rastMask[ii+2])
#                ppi = np.sum(rastPP[ii] * self.basicdata.rastMask[ii+2])
#                print('year =', (ii), (ppi - ppi_s))
          
            


            # calc IR
            # likelihood of population size given predictions times prior on parameters
            dnormPopPredNow = (np.log(self.dnorm1SD(self.basicdata.popRast[2:],
                            self.basicdata.popPred, self.basicdata.sqrt_sigma)))
            # sum only in pixels of interest (in mask)
            tmpNow = np.ma.array(dnormPopPredNow, mask=np.logical_not(self.basicdata.updateMask))
            dnormPopPredSumNow = tmpNow.sum()
            priorNow = (np.log(stats.norm.pdf(alphaPara[i],
                            self.params.alphaMean, self.params.alphaSD)))

#            print('P', P)
#            print('dnormNow and priorNow', dnormPopPredSum, priorNow)

            pnowSum = dnormPopPredSumNow + priorNow

            # evaluate proposed values
            dnormPopPredNew = (np.log(self.dnorm1SD(self.basicdata.popRast[2:],
                            popPred_s, self.basicdata.sqrt_sigma)))
            tmpNew = np.ma.array(dnormPopPredNew, mask=np.logical_not(self.basicdata.updateMask))
            dnormPopPredSumNew = tmpNew.sum()

            priorNew = (np.log(stats.norm.pdf(alphaPara_s[i],
                            self.params.alphaMean, self.params.alphaSD)))
            pnewSum = dnormPopPredSumNew + priorNew

            
#            print('PPPPPPPPPPPP', P)
#            tmp1 = tmpNew - tmpNow
#            tmp2 = np.unravel_index(tmp1.argmin(),tmp1.shape)
#            print('min indx', tmp2)
#            tmp3 = np.unravel_index(tmp1.argmax(),tmp1.shape)
#            print('max indx', tmp3)

#            print('dnorm diff', dnormPopPredNew[30,20,50] - dnormPopPredNow[30,20,50])
#            print('population Diff', popPred_s[30,20,50] - self.basicdata.popPred[30,20,50])
#            print('Diff pnowSum  pnewSum', dnormPopPredSumNew - dnormPopPredSumNow)
#            print('priorNew - priorNow', priorNew - priorNow)
#            print('dnew when now=min(now)', tmpNew[tmpNow == tmpNow.min()], tmpNow.min())
#            print('dnow when new=min(new)', tmpNow[tmpNew == tmpNew.min()], tmpNew.min())
#            print('min dnew and dnow', tmpNew.min(), tmpNow.min())
#            print('##############################################################################')
            

            pdiff = pnewSum - pnowSum

            if pdiff > 2:                       # need this because initial parameter are way off
                rValue = 1.0                      # without this step, get LogLik = inf or -inf
                zValue = 0.0
            elif pdiff < -100:
                rValue = 0.0                      # without this step, get LogLik = inf or -inf
                zValue = 1.0
            else:
                rValue = np.exp(pdiff)        # calc importance ratio
                zValue = np.random.uniform(0, 1.0, size = None)

            # if proposed parameter is better, keep it and update B0 and popPred
            if rValue > zValue:
                alphaPara[i] = alphaPara_s[i]
                self.basicdata.popPred = popPred_s.copy()
                # update only if B1
                if P == 1:
                    self.basicdata.B1_rast = bRaster_s.copy()
                    self.basicdata.alpha_b1[i] = alphaPara_s[i]
                    self.basicdata.densityDepend = DDEffect_s.copy()
                # update only if B2
                if P == 2:
                    self.basicdata.B2_rast = bRaster_s.copy()
                    self.basicdata.alpha_b2[i] = alphaPara_s[i]
                    self.basicdata.delayedDensityDepend = DDDEffect_s.copy()
        
#        poprast = self.basicdata.popRast[15]
#        predrastNow = self.basicdata.popPred[13]
#        predrastNew = popPred_s[13]
#        dnow = tmpNow[13]
#        dnew = tmpNew[13]
#        tmpdefol29 = self.basicdata.defol[29]
#        tmpdefol30 = self.basicdata.defol[30]
#        tmpdefol91 = self.basicdata.defol[91]
#        rasterutils.writeToFile(poprast, 'poprast.img')
#        rasterutils.writeToFile(predrastNow, 'predrastnow.img')
#        rasterutils.writeToFile(predrastNew, 'predrastnew.img')
#        rasterutils.writeToFile(dnow, 'dnow.img')
#        rasterutils.writeToFile(dnew, 'dnew.img')
#        rasterutils.writeToFile(tmpdefol29, 'defol29.img')
#        rasterutils.writeToFile(tmpdefol30, 'defol30.img')
#        rasterutils.writeToFile(tmpdefol29, 'defol91.img')
            
    def explore_se(self, spatialEffectRast_s, popPred_s):
        for ii in range(self.basicdata.nYears):
            se_sSumi = np.sum(spatialEffectRast_s[ii] * self.basicdata.rastMask[ii+2])
            seSumi = np.sum(self.basicdata.spatialEffectRast[ii] * self.basicdata.rastMask[ii+2])
            sumrastmask = np.sum(self.basicdata.rastMask[ii+2])
            celldiff = np.sum(np.abs(self.basicdata.rastMask[ii+2] - self.basicdata.rastMask[ii+1]))
#            print('year =', (ii), (se_sSumi - seSumi), sumrastmask, celldiff)
            ppNow = np.sum(self.basicdata.popPred[ii] * self.basicdata.rastMask[ii+2])
            ppNew = np.sum(popPred_s[ii] * self.basicdata.rastMask[ii+2])
#            print('year =', (ii), (ppNow - ppNew))


    def k_Update(self):
        """
        Update k parameters for calculating spatial effects
        """
        tmpMask = self.basicdata.maskBool[2:]
#        print('mask shp', np.shape(tmpMask))

#        k_s = self.basicdata.k
        k_s = np.exp(np.random.normal(np.log(self.basicdata.k), 0.01, size = None))    #.015
        distarray_s = rasterutils.makeDistArray(k_s,
                                self.params.se_winsize, self.params.se_halfwinsize)        
        # 3-d array of PROPOSED spatial effects
#        self.basicdata.spatialEffectRast = self.basicdata.spatialEffectFX(self.basicdata.se_distarray)
        spatialEffectRast_s = self.basicdata.spatialEffectFX(distarray_s)

        # predict pop with new k value
        popPred_s = self.basicdata.PopPredict(self.basicdata.B0_rast,
                            self.basicdata.densityDepend,
                            self.basicdata.delayedDensityDepend,
                            spatialEffectRast_s,
                            self.basicdata.weatherEffect)

        dnormPopPred = (np.log(self.dnorm1SD(self.basicdata.popRast[2:],
                            self.basicdata.popPred, self.basicdata.sqrt_sigma)))

#        self.explore_se(spatialEffectRast_s, popPred_s)
#        spatialEffectRast_s = self.basicdata.spatialEffectRast.copy()



#        print('###################')
#        print('darray', self.basicdata.se_distarray)
#        print('darray_s', distarray_s)

#        popPredNow = np.sum(self.basicdata.popPred * self.basicdata.rastMask[2:])
#        popPredNew = np.sum(popPred_s * self.basicdata.rastMask[2:])
#        print('poppred diff', popPredNow, popPredNew)

#        seNow = self.basicdata.spatialEffectRast * self.basicdata.rastMask[2:]
#        print('sum se now', np.sum(seNow))

        tmp0 = np.ma.array(dnormPopPred, mask=np.logical_not(tmpMask))    #self.basicdata.updateMask))
        dnormPopPredSum = np.sum(tmp0)
        priorNow = (np.log(stats.norm.pdf(np.log(self.basicdata.k),
                            self.params.kPriorMean, self.params.kPriorSD)))
        pnowSum = dnormPopPredSum + priorNow

        # evaluate proposed values
        dnormPopPred_s = (np.log(self.dnorm1SD(self.basicdata.popRast[2:],
                            popPred_s, self.basicdata.sqrt_sigma)))

#        seEff_s = spatialEffectRast_s * self.basicdata.rastMask[2:]
#        print('sum se new', np.sum(seEff_s))

        tmp2 = np.ma.array(dnormPopPred_s, mask=np.logical_not(tmpMask))    #self.basicdata.updateMask))
        dnormPopPredSum_s = np.sum(tmp2)
        priorNew = (np.log(stats.norm.pdf(np.log(k_s),
                            self.params.kPriorMean, self.params.kPriorSD)))
        pnewSum = dnormPopPredSum_s + priorNew

#        print('dnormPopPredSum + priorNow', dnormPopPredSum, priorNow)
#        print('dnormPopPredSum_s + priorNew', dnormPopPredSum_s, priorNew)


        pdiff = pnewSum - pnowSum
        if pdiff > 2:                       # need this because initial parameter are way off
            rValue = 1.0                      # without this step, get LogLik = inf or -inf
            zValue = 0.0
        elif pdiff < -100:
            rValue = 0.0                      # without this step, get LogLik = inf or -inf
            zValue = 1.0
        else:
            rValue = np.exp(pdiff)        # calc importance ratio
            zValue = np.random.uniform(0, 1.0, size = None)

#        print('distarr_s', distarray_s)
#        print('self.basicdata.distarray', self.basicdata.se_distarray)
#        print('dnormPopPred', dnormPopPred[18, 38:42, 103:107])
#        print('dnormPopPred_s', dnormPopPred_s[18, 38:42, 103:107])

#        print('tmp0', (tmp0[0, 30:41, 95:100]))
#        print('tmp2', (tmp2[0, 30:41, 95:100]))
#        print('tmpmask', tmpMask[0, 30:41, 95:100])

#        print('tmp0', tmp0[91, 38:42, 103:107])
#        print('tmp2', tmp2[91, 38:42, 103:107])
#        print('tmpnow', np.sum((tmp0 * 0) + 1))
#        print('tmpnnew', np.sum((tmp2 * 0) + 1))

#        print('tmp0shp', np.shape(tmp0))
#        print('tmp2shp', np.shape(tmp2))

#        print('self.basicdata.popPred', self.basicdata.popPred[18, 38:42, 103:107])
#        print('popPred_s', popPred_s[18, 38:42, 103:107])
#        print('self.basicdata.se', self.basicdata.spatialEffectRast[18, 38:42, 103:107])

#        print('se_s', spatialEffectRast_s[18, 38:42, 103:107]) 

#        print('k_s', k_s)
#        print('r-z', rValue - zValue)
#        print('dnorm and dnorm_s', dnormPopPredSum , dnormPopPredSum_s)
#        print('priornow and priornew', priorNow, priorNew)
#        print('###################')

        # if proposed parameter is better, keep it and update 
        if rValue > zValue:
            self.basicdata.k = k_s   
            self.basicdata.spatialEffectRast = spatialEffectRast_s.copy()
#            self.basicdata.spatialEffectPsi = spatialEffectPsi_s.copy() 
            self.basicdata.popPred = popPred_s.copy()
            self.basicdata.se_distarray = distarray_s.copy()

###############
########
##################################   Removed psi Update
#####
########
##############

    def gamma_Update(self):
        """
        Update gamma parameters for calculating Weather effect rasters
        """
        ## loop through gamm parameters
        npara = len(self.basicdata.gamma)
        for i in range(npara):
            gamma_s = self.basicdata.gamma.copy()
            # draw new proposal from normal
            gamma_s[i] = np.random.normal(self.basicdata.gamma[i], 0.02)
            weatherGamma_s = self.basicdata.weatherGamma.copy()
            # proposed weather gamma 3-d array for gamma[i]
            weatherGamma_s[i] = gamma_s[i] * self.basicdata.weatherVariables[i]
            # get overall weather effect with gamma_s[i] - sum across variables
            weatherEffect_s = self.basicdata.calcWeatherEffect(weatherGamma_s)
            # pred population with gamma_s and weather effects
            popPred_s = (self.basicdata.PopPredict(self.basicdata.B0_rast,
                            self.basicdata.densityDepend,
                            self.basicdata.delayedDensityDepend,
                            self.basicdata.spatialEffectRast,
                            weatherEffect_s))
            dnormPopPred = (np.log(self.dnorm1SD(self.basicdata.popRast[2:],
                            self.basicdata.popPred, self.basicdata.sqrt_sigma)))
            tmp = np.ma.array(dnormPopPred, mask=np.logical_not(self.basicdata.updateMask))
            dnormPopPredSum = tmp.sum()
            priorNow = (np.log(stats.norm.pdf(self.basicdata.gamma[i],
                            self.params.gammaPriorMean, self.params.gammaPriorSD)))
            pnowSum = dnormPopPredSum + priorNow

            # evaluate proposed values
            dnormPopPred = (np.log(self.dnorm1SD(self.basicdata.popRast[2:],
                            popPred_s, self.basicdata.sqrt_sigma)))
            tmp = np.ma.array(dnormPopPred, mask=np.logical_not(self.basicdata.updateMask))
            dnormPopPredSum = tmp.sum()

            priorNew = (np.log(stats.norm.pdf(gamma_s[i],
                            self.params.gammaPriorMean, self.params.gammaPriorSD)))
            pnewSum = dnormPopPredSum + priorNew

            pdiff = pnewSum - pnowSum
            if pdiff > 2:                       # need this because initial parameter are way off
                rValue = 1.0                      # without this step, get LogLik = inf or -inf
                zValue = 0.0
            elif pdiff < -100:
                rValue = 0.0                      # without this step, get LogLik = inf or -inf
                zValue = 1.0
            else:
                rValue = np.exp(pdiff)        # calc importance ratio
                zValue = np.random.uniform(0, 1.0, size = None)

            # if proposed parameter is better, keep it and update B0 and popPred
            if rValue > zValue:
                self.basicdata.gamma[i] = gamma_s[i].copy()
                self.basicdata.weatherGamma[i] = weatherGamma_s[i]
                self.basicdata.weatherEffect = weatherEffect_s.copy()
                self.basicdata.popPred = popPred_s.copy()


########            Main mcmc function
########
    def mcmcFX(self):
        cc = 0
        for g in range(self.params.ngibbs * self.params.thinrate + self.params.burnin):

            # update sigma
            self.vupdate()

            self.popUpdateAll()

####            self.updateDetectError()

#            self.updateSigmaDetect()

#            self.updateRho()

####            self. updateDelta0()

            self.alphaB0_Update()

            # updata alphas for Beta1
            self.alphaBeta12_Update(self.basicdata.alpha_b1, self.basicdata.B1_rast, 
                                     self.basicdata.b1_climateVariables, 1)
            # updata alphas for Beta2
            self.alphaBeta12_Update(self.basicdata.alpha_b2, self.basicdata.B2_rast, 
                                     self.basicdata.b2_climateVariables, 2)

            # update k parameter
            self.k_Update()

            self.gamma_Update()
            
            if g in self.params.keepseq:
                self.B0gibbs[cc] = self.basicdata.alpha_b0
                self.B1gibbs[cc] = self.basicdata.alpha_b1
                self.B2gibbs[cc] = self.basicdata.alpha_b2
                self.kgibbs[cc] = self.basicdata.k
#                self.psigibbs[cc] = self.basicdata.psi
                self.gammagibbs[cc] = self.basicdata.gamma
                self.siggibbs[cc] = self.basicdata.sigma
#                self.sigmaDetectgibbs[cc] = self.basicdata.sigmaDetect 
#                self.rhogibbs[cc] = self.basicdata.rho
#                self.delta0gibbs[cc] = self.basicdata.delta0

                

                popG = np.array([self.basicdata.popRast[34, 20, 50],
                                self.basicdata.popPred[32, 20, 50],
                                self.basicdata.popRast[20, 40, 105],
                                self.basicdata.popPred[18, 40, 105]])
#                                self.basicdata.popRast[62, 50, 80],
#                                self.basicdata.popPred[60, 50, 80]])

#                ErrDetect = np.array([self.basicdata.errorDetRast[32, 20, 50],
#                                self.basicdata.errorDetRast[82, 25, 55],
#                                self.basicdata.errorDetRast[60, 50, 80],
#                                self.basicdata.errorDetRast[18, 40, 105]])                                
#                                self.basicdata.errorDetRast[76, 35, 100],
#                                self.basicdata.errorDetRast[12, 23, 95]])

                self.popgibbs[cc] = popG
#                self.errDetectgibbs[cc] = ErrDetect
#                self.errByYeargibbs[cc] = self.basicdata.errByYear.flatten()
                cc = cc + 1
#                print('cc', cc)
#                print('cc', cc, 'errbyyear mean', np.mean(self.basicdata.errByYear))
#                print(self.basicdata.rastMask[12, 30, 50], self.basicdata.defol[10, 30, 50])
#                print(self.basicdata.rastMask[72, 35, 45], self.basicdata.defol[70, 35, 40])
#                print(self.basicdata.rastMask[34, 20, 50], self.basicdata.popRast[34, 20, 50], self.basicdata.popPred[34, 20, 50])

########            Pickle results to directory
########




class Gibbs(object):
    def __init__(self, mcmcobj):
        self.B0gibbs = mcmcobj.B0gibbs
        self.B1gibbs = mcmcobj.B1gibbs
        self.B2gibbs = mcmcobj.B2gibbs
        self.kgibbs = mcmcobj.kgibbs
#        self.psigibbs = mcmcobj.psigibbs
        self.gammagibbs = mcmcobj.gammagibbs
        self.siggibbs = mcmcobj.siggibbs
#        self.sigmaDetectgibbs = mcmcobj.sigmaDetectgibbs
#        self.rhogibbs = mcmcobj.rhogibbs
#        self.delta0gibbs = mcmcobj.delta0gibbs 
        self.popgibbs = mcmcobj.popgibbs
#        self.errDetectgibbs = mcmcobj.errDetectgibbs
#        self.errByYeargibbs = mcmcobj.errByYeargibbs

########            Main function
#######
def main(params, basicdatapickle=None, poprastpickle=None):

    #np.seterr(all='raise')

    defolpath = os.getenv('DEFOLPROJDIR', default='.')
    # paths and data to read in
    defolWeathDatFile = os.path.join(defolpath,'defolWeath2010.csv')
    climateDatFile = os.path.join(defolpath,'climPCA3.csv')
    landDatFile = os.path.join(defolpath,'landcover2010.csv')

    # initiate basicdata class and object when do not read in previous results
    if basicdatapickle is None:
        basicdata = BasicData(defolWeathDatFile, climateDatFile, params, landDatFile)

    else:
        # read in pickled results (basicdata) from a previous run
#        inputBasicdata = os.path.join(defolpath, picklefile)
        fileobj = open(basicdatapickle, 'rb')
        basicdata = pickle.load(fileobj)
        fileobj.close()

    if poprastpickle is not None:
        fileobj = open(poprastpickle, 'rb')
        basicdata.popRast = pickle.load(fileobj)
        fileobj.close()


    # initiate mcmc class and object
    mcmcobj = MCMC(params, basicdata)

    # run mcmcFX - gibbs loop
    mcmcobj.mcmcFX()

    gibbsobj = Gibbs(mcmcobj)

    # pickle basic data from present run to be used to initiate new runs
    outBasicdata = os.path.join(defolpath,'out_basicdata_NoDetect.pkl')
    fileobj = open(outBasicdata, 'wb')
    pickle.dump(basicdata, fileobj)
    fileobj.close()

    # pickle mcmc results for post processing in gibbsProcessing.py
    outGibbs = os.path.join(defolpath,'out_gibbs_NoDetect.pkl')
    fileobj = open(outGibbs, 'wb')
    pickle.dump(gibbsobj, fileobj)
    fileobj.close()

    # pickle basicdata.popRast
#    population = basicdata.popRast
#    outPop = os.path.join(defolpath,'out_population.pkl')
#    fileobj = open(outPop, 'wb')
#    pickle.dump(population, fileobj)
#    fileobj.close()


if __name__ == '__main__':
    main()


