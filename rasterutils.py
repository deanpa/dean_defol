#!/usr/bin/env python
"""
Raster functions to assist with defoliation
"""
import numpy
#from osgeo import gdal
from numba import jit

RES = 10000.0 # in metres
MIN_X = 56571.67 - (RES / 2.0)
MAX_X = 1546572 + (RES / 2.0)
MIN_Y = 390408.8 - (RES / 2.0)
MAX_Y = 1280409 + (RES / 2.0)

N_XPIX = int(numpy.ceil((MAX_X - MIN_X) / RES))
N_YPIX = int(numpy.ceil((MAX_Y - MIN_Y) / RES))

DEFAULT_POPMIN = -5
DEFAULT_POPMAX = 0

@jit
def makeDistArray(k, winsize, halfwinsize):
    """
    Pre calculate this so we don't have to do it each time
    """
    distarray = numpy.empty((winsize, winsize))   #, numpy.int)
    for x in range(winsize):
        for y in range(winsize):
            distx = (x - halfwinsize) * RES
            disty = (y - halfwinsize) * RES
            dist = numpy.sqrt(distx*distx + disty*disty)
            distarray[y, x] = dist**k
    return distarray

def createInitialPopulation(popmin=DEFAULT_POPMIN, popmax=DEFAULT_POPMAX):
    """
    Create an intial population raster drawn from a uniform distribution
    with the given min and max
    """
    popn = numpy.random.uniform(low=popmin, high=popmax, size=(N_YPIX, N_XPIX))
    return popn

def getColsAndRows(easting, northing):
    """
    given an array of eastings and an array of northings
    return the cols and rows
    """
    col = numpy.round((easting - MIN_X) / RES).astype(numpy.int)
    row = numpy.round((MAX_Y - northing) / RES).astype(numpy.int)
    return col, row

def writeToFile(raster, fname):
    """
    Write a raster to a given filename
    Format is ERDAS Imagine .img format
    """
    driver = gdal.GetDriverByName('HFA')
    nbands = 1
    if raster.ndim == 3:
        nbands = raster.shape[0]

    ds = driver.Create(fname, N_XPIX, N_YPIX, nbands, gdal.GDT_Float32)
    geotransform = [MIN_X, RES, 0, MAX_Y, 0, -RES]
    ds.SetGeoTransform(geotransform)

    if nbands == 1:
        band = ds.GetRasterBand(1)
        band.WriteArray(raster)
    else:
        for bandcount in range(nbands):
            band = ds.GetRasterBand(bandcount+1)
            band.WriteArray(raster[bandcount])

    del ds

def readFromFile(fname):
    """
    Read the population data out of the first band
    of the named file
    """
    ds = gdal.Open(fname)
    band = ds.GetRasterBand(1)
    data = band.ReadAsArray()
    del ds
    return data

@jit
def calculateSpatialEffect(raster, mask, distarray, halfwinsize, logPlus):
    """
    return a spatial effect raster se1 from the input raster and mask
    Makes use of distance and K caching (see makeDistArray)
    """
    se1_out = numpy.zeros_like(raster)
    (ysize, xsize) = se1_out.shape
    for x in range(xsize):
        for y in range(ysize):
            if mask[y, x] != 0:
                # offset into dist array - deal with top and left edges
                xdistoff = 0
                ydistoff = 0
                # top left x
                tlx = x - halfwinsize
                if tlx < 0:
                    xdistoff = -tlx
                    tlx = 0
                tly = y - halfwinsize
                if tly < 0:
                    ydistoff = -tly
                    tly = 0
                brx = x + halfwinsize
                if brx > xsize - 1:
                    brx = xsize - 1
                bry = y + halfwinsize
                if bry > ysize - 1:
                    bry = ysize - 1

                se1 = 0.0
                #print(x, y, tlx, brx, tly, bry)
                for cx in range(tlx, brx+1):
                    for cy in range(tly, bry+1):
                        if mask[cy, cx] != 0:
                            dist = distarray[ydistoff + cy - tly, xdistoff + cx - tlx]
                            if dist != 0:
                                se1 += raster[cy, cx] / dist
                se1_out[y, x] = numpy.log(se1 + logPlus)
    return se1_out

if __name__ == '__main__':
    raster = createInitialPopulation()
    mask = numpy.loadtxt('mask.txt')
    #writeToFile(raster, 'test.img')

    #print(readFromFile('test.img'))
    dist = makeDistArray(0.5, 7, 3)
    import time
    start = time.time()
    a = calculateSpatialEffect(raster, mask, dist, 3)
    end = time.time()
    print(end - start)
    start = time.time()
    dist = makeDistArray(0.1, 7, 3)
    end = time.time()
    print(end - start)
    start = time.time()
    a = calculateSpatialEffect(raster, mask, dist, 3)
    end = time.time()
    print(end - start)
    print(a)

    numpy.savetxt('raster.txt', raster, fmt='%.2f')
    numpy.savetxt('dist.txt', dist, fmt='%.2f')
    numpy.savetxt('se.txt', a, fmt='%.2f')
