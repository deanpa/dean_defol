#!/usr/bin/env python

"""
Example inheritance script for the animal movements group
"""

import numpy

class MyBaseClass(object):
    """
    Class derived from 'object' (which all Python
    objects are originally). 
    """
    def __init__(self):
        """
        Constructor. Sets member variables to None
        """
        self.a = None
        self.b = None

    def doCalculation(self):
        """
        Main part of this class. Does a fancy calculation
        and returns the result.
        """
        product = self.a * self.b
        return product.mean()

    def readValues(self):
        """
        To be overridden by derived classes to read
        data into self.a and self.b
        """
        # just raise an error for now
        raise NotImplementedError("readValues needs to be implemented in derived class")

class MyRandomClass(MyBaseClass):
    """
    Class derived from MyBaseClass. Implements its own version
    of readValues, but has all the other bits of MyBaseClass
    """
    def __init__(self):
        """
        Constructor - calls the base class constructor
        so the member variables are initialised
        """
        MyBaseClass.__init__(self)

    def readValues(self):
        """
        overidden implementation that generates a and b from some
        random data
        """
        self.a = numpy.random.randint(0, 10, size=5)
        self.b = numpy.random.randint(0, 20, size=5)

def test():
    """
    My test function
    """
    # try creating the derived class
    derived = MyRandomClass()

    # this will called the overidden implementation
    derived.readValues() 

    # this will call the base class implementation
    result = derived.doCalculation()
    print('result = ', result)

    # try the same with the base class
    base = MyBaseClass()
    
    # will call the MyBaseClass implementation which 
    # will raise an exception
    base.readValues()
        
if __name__ == '__main__':
    # if being run as a script (rather than being imported)
    # run my tests
    test()
