#!/usr/bin/env python

import os
import numpy as np
from numba import autojit
import pickle


class ClimateData(object):
    def __init__(self, climateFname):
        """
        Object to read in climate data from PCA
        """

        # read in climate data
        self.climate = np.genfromtxt(climateFname,  delimiter=',', names=True,
            dtype=['i8', 'f8', 'f8'])

        self.ax1 = self.climate['ax1']
        self.ax2 = self.climate['ax2']
        self.ndat = len(self.ax2)
        self.covDat = np.empty((self.ndat, 3))
        self.covDat[:,0] = 1.0
        self.covDat[:,1] = self.ax1
        self.covDat[:,2] = self.ax2
        #############################
        # run functions
        ######

        self.gendata()
        self.getB1()
#        self.getB2()
        ######
        ############################

    def gendata(self):
        """
        gen coeff for a0, a1, a2, a3, a4, a5
        """

        self.values = 50
        self.ca3_seq = np.linspace(-1.2, .1, num = self.values)
        self.ca4_seq = np.linspace(-.12, .12, num = self.values)     #np.arange(-.3, .3, step=.01)
        self.ca5_seq = np.linspace(-.14, .14, num = self.values)     #np.arange(-.3, .3, step=.01)

        self.variables = 3
        self.ntot = np.int(self.values**self.variables)
        self.indxDat = np.zeros(self.ntot, dtype = int)
        print('ntot', self.ntot)
        self.alphaDDD = np.empty((self.ntot, 3))
        self.alphaDDD[:,0] = np.repeat(self.ca3_seq, self.ntot/self.values)
        self.alphaDDD[:,1] = np.tile(np.repeat(self.ca4_seq, self.values), self.values)
        self.alphaDDD[:,2] = np.tile(self.ca5_seq, self.ntot / self.values)

        # parameter seq for DD 
        self.ca0_seq = np.linspace(-.1, 2, num = self.values)
        self.ca1_seq = np.linspace(-1, 1, num = self.values)     
        self.ca2_seq = np.linspace(-1, 1, num = self.values)     

        self.variablesB1 = 3
        self.ntotB1 = np.int(self.values**self.variablesB1)
        self.indxDatB1 = np.zeros(self.ntotB1, dtype = int)
        print('ntotB1', self.ntotB1)
        self.alphaDD = np.empty((self.ntotB1, 3))
        self.alphaDD[:,0] = np.repeat(self.ca0_seq, self.ntotB1/self.values)
        self.alphaDD[:,1] = np.tile(np.repeat(self.ca1_seq, self.values), self.values)
        self.alphaDD[:,2] = np.tile(self.ca2_seq, self.ntotB1 / self.values)


#    @autojit
    def getB1(self):
        """
        Get alpha parameters for Beta1 (DD)
        """
        for i in range(self.ntotB1):
            btmpB1 = self.alphaDD[i]
            B1 = np.dot(self.covDat, btmpB1)
            condition1 = np.float32(len(B1[B1 < -1]))
            condition2 = np.float32(len(B1[B1 > 0.0]))
            nout = condition1 + condition2
            if nout > 0.0:
                self.indxDatB1[i] = np.int(1)
        self.alphaDD = self.alphaDD[self.indxDatB1 == np.int(0)]
        print('aDD shp', np.shape(self.alphaDD))
        print('min', np.min(self.alphaDD, axis = 0))
        print('max', np.max(self.alphaDD, axis = 0))


#    @autojit
    def getB2(self):
        """
        Get alpha parameters for Beta2 (DDD)
        """
        for i in range(self.ntot):
            btmp = self.alphaDDD[i]
            B2 = np.dot(self.covDat, btmp)
            condition1 = np.float32(len(B2[B2 < -1]))
            condition2 = np.float32(len(B2[B2 > 0.0]))
            nout = condition1 + condition2
            if nout > 0.0:
                self.indxDat[i] = np.int(1)
        self.alphaDDD = self.alphaDDD[self.indxDat == np.int(0)]

#        print('aDDD shp', np.shape(self.alphaDDD))
#        print('min', np.min(self.alphaDDD, axis = 0))
#        print('max', np.max(self.alphaDDD, axis = 0))
            

#            period = (2*np.pi) / np.arccos(btmp[1] / 2 / np.sqrt(-btmp[2]))

#            print('B2', B2[0:10])
#            print('period', period[0:10]) 



class ClimateParameters(object):
    def __init__(self, climatedata):
        self.alphaDDD = climatedata.alphaDDD




########            Main function
#######
def main():

    defolpath = os.getenv('DEFOLPROJDIR', default='.')
    # paths and data to read in
    climateDatFile = os.path.join(defolpath,'climate.csv')

    climatedata = ClimateData(climateDatFile)

    climateparameters = ClimateParameters(climatedata)

    # pickle basic data from present run to be used to initiate new runs
    outClimateParameters = os.path.join(defolpath,'out_climateparameters.pkl')
    fileobj = open(outClimateParameters, 'wb')
    pickle.dump(climateparameters, fileobj)
    fileobj.close()


if __name__ == '__main__':
    main()

