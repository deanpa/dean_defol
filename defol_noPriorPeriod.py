#!/usr/bin/env python

import os
import params
from scipy import stats
#from scipy.special import gammaln
#from scipy.special import gamma
import numpy as np
import numpy.ma as ma
import rasterutils
#import pylab as P
#from numba import autojit
import pickle
from lcrimageutils import mdl

def logit(x):
    """
    Function (logit) to convert probability to real number
    """
    return np.log(x) - np.log(1 - x)

def inv_logit(x):
    """
    Function to convert real number to probability
    """
    return np.exp(x) / (1 + np.exp(x))


class BasicData(object):
    def __init__(self, defolFname, climateFname, params):
        """
        Object to read in defol-weather data, and climate data from PCA
        Create initial pop raster for 1910 - 2003.
        We estimate '10-'11 because of delayed density dependence (AR2 model)
        """
        self.params = params
        
        ##############
        ###############      Initial values parameters for updating
        # ############       Use values from params in case don't use pickled initial values
        # k is decay parameter for spatial effect
        self.k = self.params.k
        # make dist array for neighbourhood window incorporating k
        self.se_distarray = rasterutils.makeDistArray(self.k,
                                self.params.se_winsize, self.params.se_halfwinsize)
        # alphas for b0 - intrinsic rate of growth
        self.alpha_b0 = self.params.alpha_b0.copy()                # intercept and ax1 only
        # alphas for b1 - density dependence
        self.alpha_b1 = self.params.alpha_b1.copy()             # intercept, ax1, and ax2
        # alphas for b2 - delayed density dependence
        self.alpha_b2 = self.params.alpha_b2.copy()
        # gamma parameter for weather variables
        self.gamma = self.params.gamma.copy()

        # sigma variance parameter of population size
        self.sigma = self.params.sigma
        self.sqrt_sigma = self.params.sqrt_sigma

        # add to se to take log
        self.logPlus = self.params.logPlus          # 0.001

        #################
        #################

        # read in defoliation and weather data
        self.defolWeath = np.genfromtxt(defolFname,  delimiter=',', names=True,
            dtype=['i8', 'i8', 'i8', 'f8', 'f8', 'f8', 'f8', 'f8', 'f8'])

        # read in climate data
        self.climate = np.genfromtxt(climateFname,  delimiter=',', names=True,
            dtype=['i8', 'f8', 'f8'])

        easting = self.defolWeath['x']
        northing = self.defolWeath['y']
        self.col, self.row = rasterutils.getColsAndRows(easting, northing)
        self.years = np.unique(self.defolWeath['year'])
        self.nYears = np.size(self.years)
        self.seqI = np.arange(2, self.nYears)
        self.ndat = len(easting)

        self.makeMask()                                     # mask of cells to analyse
#        print('nRandInYr', self.nRandInYr)
#        print('datID', len(self.datID), self.datID[0:20])

        self.makeDefolWeath()                               # multiple 3-d arrays of defol and weather data
        self.makePopRast()                                  # 3-d array of population data
        self.proposeNewPop()

        
        # 3-d array of spatial effects
        self.spatialEffectRast = self.spatialEffectFX(self.se_distarray)
#        self.spatialEffectPsi = self.spatialEffectRast
        
        # 4-d array for climate variables ax1 and ax2 for B0, B1, and B2
        # no updating of this
        (self.b0_climateVariables, self.b1_climateVariables, 
            self.b2_climateVariables) = self.makeClimateRast()                            


        # make 3-d arrays of Betas
        # have to update when updata alphas
        self.B0_rast = self.makeBetaRasters(self.alpha_b0, self.b0_climateVariables)
        self.B1_rast = self.makeBetaRasters(self.alpha_b1, self.b1_climateVariables)
        self.B2_rast = self.makeBetaRasters(self.alpha_b2, self.b2_climateVariables)
        
        #calculate the density dependence effect
        self.densityDepend = self.calcDensityDepend(self.B1_rast, self.popRast[1:-1])

        #calculate the DELAYED density dependence effect
        self.delayedDensityDepend = self.calcDelayedDensityDepend(self.B2_rast, self.popRast[0:-2])


        # 3 or 4-d array of weather variables multiplied times gamma
        # one stack for each variable
        self.calcWeatherGamma()

        # one 3-d array of total weather effect (sum of calcWeatherGamma)
        self.weatherEffect = self.calcWeatherEffect(self.weatherGamma)

        # make 3-d predicted population
        self.popPred = self.PopPredict(self.B0_rast, 
                        self.densityDepend,
                        self.delayedDensityDepend, 
                        self.spatialEffectRast,              #Psi,
                        self.weatherEffect)

#########################################
#########################################
#
#   Below are functions for Basicdata
#
########################################
########################################

    def makeMask(self):
        """
        Function to make 3-d mask of cells to analyse: ones and zeros
        Make a mask for each year.
        Note that 1910-1953 have Ontario only. Post '53 have MINN and ONT
        """
        self.rastMask = np.zeros(((self.nYears + 2), rasterutils.N_YPIX, rasterutils.N_XPIX), dtype=np.uint8)
        self.zerosRaster = self.rastMask[0]
        self.datID = np.empty(self.ndat, dtype = int)        # will be used to propose new pop values
        self.nRandInYr = np.zeros(self.nYears, dtype = int)  # number of random pops to propose each year
        # make mask rasters for years 1910 - 11
        for i in range(2):
            datMask = self.defolWeath['year'] == 1912
            col = self.col[datMask]
            row = self.row[datMask]
            self.rastMask[i, row, col] = 1
        # make mask rasters for years 1912-2003
        for i in range(self.nYears):
            datMask = self.defolWeath['year'] == self.years[i]
            col = self.col[datMask]
            row = self.row[datMask]
            self.rastMask[(i + 2), row, col] = 1
            ntmp = len(col)                                 # len of data in year i
            dtmp = np.arange(ntmp)                          # data seq for year i
            self.datID[datMask] = dtmp        # append
            self.nRandInYr[i] = np.int(0.5 * ntmp)

        print('l datID and nRandID', len(self.datID), len(self.nRandInYr))
        print('ndat', self.ndat)
        self.maskBool = self.rastMask == 1

    def makeDefolWeath(self):
        """
        function to make 3-d arrays of defol data and  weather for 1912-2003
        """
        # 3-d arrays with zeros for defol data
        self.defol = np.zeros((self.nYears, rasterutils.N_YPIX, rasterutils.N_XPIX), dtype=np.uint8)
        defolDataAll = self.defolWeath['defol']                                      # defol data 1-d
        defolDataAll = np.ceil(defolDataAll)                                         # ceiling of defol
        defolDataAll = defolDataAll.astype(np.integer)                               # make integer

        # 3-d arrays for weather with zeros to fill in
        minWinTemp = np.zeros((self.nYears, rasterutils.N_YPIX, rasterutils.N_XPIX))
        minSumTemp = np.zeros((self.nYears, rasterutils.N_YPIX, rasterutils.N_XPIX))
        pcpJune = np.zeros((self.nYears, rasterutils.N_YPIX, rasterutils.N_XPIX))

        # 1-d array with weather data
        minWT = self.defolWeath['minwT']                   
        minST = self.defolWeath['minsT']
        pcpJ = self.defolWeath['pcpJ']

        sc_minWT = (minWT - np.mean(minWT)) / np.std(minWT)
        sc_minST = (minST - np.mean(minST)) / np.std(minST)
        sc_pcpJ = (pcpJ - np.mean(pcpJ)) / np.std(pcpJ)


        # loop to populate defol and weath rasters
        for i in range(self.nYears):
            yearMask = self.defolWeath['year'] == self.years[i]
            defolYear = defolDataAll[yearMask]
            col = self.col[yearMask]
            row = self.row[yearMask]
            self.defol[i, row, col] = defolYear            # defoliation rasters

            minwtyear = sc_minWT[yearMask]
            minstyear = sc_minST[yearMask]
            pcpjyear = sc_pcpJ[yearMask]
            
            minWinTemp[i, row, col] = minwtyear       # weather rasters
            minSumTemp[i, row, col] = minstyear
            pcpJune[i, row, col] = pcpjyear
        
        # a 4-d raster of weather 
        WeatherRast = np.array([minWinTemp, minSumTemp, pcpJune])
        self.weatherVariables = WeatherRast[self.params.WeatherIndx]


    def makePopRast(self):
        """
        function to make 3-d array of latent population size from 1910 - 2003
        Note that population is an unobserved latent variable that we are estimating
        These are initial values, in this case they are random.
        """
        # pop 3-d raster with zeros to be filled in
        self.popRast = np.zeros(((self.nYears + 2), rasterutils.N_YPIX, rasterutils.N_XPIX))

        # temp rasters to set initial population values for defoliated cells
        lowDefolRast = np.empty((rasterutils.N_YPIX, rasterutils.N_XPIX))
        lowDefolRast.fill(3)
        highDefolRast = np.empty((rasterutils.N_YPIX, rasterutils.N_XPIX))
        highDefolRast.fill(7)

        # temp rasters to set initial population values for non-defoliated cells
        lowNonDefolRast = np.empty((rasterutils.N_YPIX, rasterutils.N_XPIX))
        lowNonDefolRast.fill(0)
        highNonDefolRast = np.empty((rasterutils.N_YPIX, rasterutils.N_XPIX))
        highNonDefolRast.fill(3)

        # loop to populate the population rasters for 1910-1911
        for i in range(2):
            maskDefol = self.defol[0] == 1
            low = np.where(maskDefol, lowDefolRast, lowNonDefolRast)
            high = np.where(maskDefol, highDefolRast, highNonDefolRast)
            self.popRast[i] = np.random.uniform(low=low, high=high)

        # loop to populate the population rasters 1912-2003
        for i in range(self.nYears):
            maskDefol = self.defol[i] == 1
            low = np.where(maskDefol, lowDefolRast, lowNonDefolRast)
            high = np.where(maskDefol, highDefolRast, highNonDefolRast)
            self.popRast[i+2] = np.random.uniform(low=low, high=high)

        # multiple times yearly mask to put zeros where masked out
        self.popRast = self.popRast * self.rastMask  
        self.expPopRast = np.exp(self.popRast) * self.rastMask


#    def proposeNewPop(self):
#        """
#        Function to propose new latent population estimates in 3-d array
#        """
#        # propose new variate from normal distribution with sd "searchPopVar"
#        self.popRast_s = np.random.normal(self.popRast, self.params.searchPopVar)
#        # multiply times mask to put zeros outside of area of interest
#        self.popRast_s = self.popRast_s * self.rastMask
#        expPop = np.exp(self.popRast_s) 
#        self.expPopRast_s = expPop * self.rastMask


    def proposeNewPop(self):
        """
        Function to propose new latent population estimates in 3-d array
        """
        # propose new variate from normal distribution with sd "searchPopVar"
        # loop to propose the population rasters for 1910-1911
        self.popRast_s = self.popRast.copy()
        for i in range(2):
            datMask = self.defolWeath['year'] == 1912
            col = self.col[datMask]
            row = self.row[datMask]
            dID = self.datID[datMask]                                       # seq data for yr
            nrand = self.nRandInYr[0]                                            # number to propose
            rid = np.random.choice(dID, nrand, replace = False)             # cell to propose
            randV = np.random.normal(0, self.params.searchPopVar, nrand)    # random variates
            col2 = col[rid]                                                 # col and row id to propose
            row2 = row[rid]
            self.popRast_s[i, row2, col2] = self.popRast[i, row2, col2] + randV # add random variate
        # loop through 1912 - 2003
        for i in self.seqI:
            datMask = self.defolWeath['year'] == self.years[i-2]
            col = self.col[datMask]
            row = self.row[datMask]
            dID = self.datID[datMask]                                       # seq data for yr
            nrand = self.nRandInYr[i-2]                                            # number to propose
            rid = np.random.choice(dID, nrand, replace = False)             # cell to propose
            randV = np.random.normal(0, self.params.searchPopVar, nrand)    # random variates
            col2 = col[rid]                                                 # col and row id to propose
            row2 = row[rid]
            self.popRast_s[i, row2, col2] = self.popRast[i, row2, col2] + randV # add random varia
        # multiply times mask to put zeros outside of area of interest
        self.popRast_s = self.popRast_s * self.rastMask
        expPop = np.exp(self.popRast_s) 
        self.expPopRast_s = expPop * self.rastMask


    def spatialEffectFX(self, distarray):
        """
        make spatial effects 3-d array for years 1912 - 2003
        See model doc to see that neighbours at t-1 influence a cell at t
        """
        # empty 3-d spatial effects raster to be filled in.
        spatialEffect = np.empty((self.nYears, rasterutils.N_YPIX, rasterutils.N_XPIX))

        for i in range(self.nYears):
            # get spatial effect for year i from population in year i-1
            # note that there are popRast for 1910 on and spatialEff rast 1912 on.    
            spatialEffect[i] = rasterutils.calculateSpatialEffect(self.expPopRast[i+1], 
                self.rastMask[i+2], distarray, self.params.se_halfwinsize)
            spatialEffect[i] = np.log(spatialEffect[i] + self.logPlus)
        return spatialEffect

    def makeClimateRast(self):
        """
        Make 3-d arrays for the two climate PCA variables (ax1 and ax2)
        Done for years 1912 - 2003.
        No updating
        """
        # create temp mask to get raster rows and col to populate rasters below
        datMask = self.defolWeath['year'] == 1960       # post 1954 template include ONT and MN
        col = self.col[datMask]
        row = self.row[datMask]

        # make ax1 climate 3-d arrays with zeros
        ax1Rast = np.zeros((self.nYears, rasterutils.N_YPIX, rasterutils.N_XPIX))
        # scale the ax1 to mean = 0 and sd = 1
        scaleAX1 = (self.climate['ax1'] - np.mean(self.climate['ax1'])) / np.std(self.climate['ax1'])
        ax1Rast[..., row, col] = scaleAX1
        ax1Rast = ax1Rast * self.rastMask[2:]

        # 3-d array for climate ax2
        ax2Rast = np.zeros((self.nYears, rasterutils.N_YPIX, rasterutils.N_XPIX))
        # scale ax2 mean = 0 sd = 1
        scaleAX2 = (self.climate['ax2'] - np.mean(self.climate['ax2'])) / np.std(self.climate['ax2'])
        ax2Rast[..., row, col] = scaleAX2
        ax2Rast = ax2Rast * self.rastMask[2:]        

        # make a 4-d array for potential use in making beta rasters
        # model variables are indexed in params object
        climateRast = np.array([ax1Rast, ax2Rast])                         
        b0_climateVariables = climateRast[self.params.b0_climateIndx]
        b1_climateVariables = climateRast[self.params.b1_climateIndx]
        b2_climateVariables = climateRast[self.params.b2_climateIndx]

        return (b0_climateVariables, b1_climateVariables, b2_climateVariables)


    def makeBetaRasters(self, alpha, climateVariables):
        """
        make 3-d rasters for B0, B1, and B2
        Betas in equation 5
        Use to update alpha parameters.
        """
        # Cycle thru climate 3-d arrays for B0 to get B0_raster
        Beta_rast = np.zeros((self.nYears, rasterutils.N_YPIX, rasterutils.N_XPIX))
        for i in range(len(alpha) - 1):
            np.add(Beta_rast, (alpha[i + 1] * climateVariables[i]), out = Beta_rast)
#            Beta_rast += alpha[i + 1] * climateVariables[i]
        # Add intercept alpha value
        np.add(Beta_rast, alpha[0], out = Beta_rast)
        return (Beta_rast)


    def calcDensityDepend(self, B1, popRast):
        """
        Calculate the density dependence effect
        Use when update alpha for Beta1, or update population
        """
        DD = B1 * popRast
        return DD

    def calcDelayedDensityDepend(self, B2, popRast):
        """
        Calculate the DELAYED density dependence effect
        Use when update alpha for Beta2, or update population
        """
        DDD = B2 * popRast
        return DDD


    def calcWeatherGamma(self):
        """
        multiply gamma times weather variable and keep as 3 or 4-d array
        """
        self.weatherGamma = self.weatherVariables.copy()
        for i in range(len(self.gamma)):
            self.weatherGamma[i] = self.gamma[i] * self.weatherVariables[i]
        

    def calcWeatherEffect(self, weatherGamma):
        """
        Calc 3-d weather effect using variables and gamma parameters
        """
        return np.sum(weatherGamma, axis = 0)


    def PopPredict(self, B0, DD, DDD, SE, WE):
        """
        calc population prediction for all years and cells
        This is prediction of population (latent variable) using parameters
        Equation 5
        """
        tmpPopPred = B0 + DD + DDD + SE + WE
        return(tmpPopPred)


class MCMC(object):
    def __init__(self, params, basicdata):

        self.basicdata = basicdata




        self.params = params

        # storage arrays for the alpha_B0, B1 and B1 parameter for climate effects  
        self.B0gibbs = np.zeros([self.params.ngibbs, len(self.basicdata.alpha_b0)]) #rate of growth
        self.B1gibbs = np.zeros([self.params.ngibbs, len(self.basicdata.alpha_b1)]) # density dependence
        self.B2gibbs = np.zeros([self.params.ngibbs, len(self.basicdata.alpha_b2)]) # delayed density dependence

        self.kgibbs = np.zeros(self.params.ngibbs)          # spatial effect decay
        self.siggibbs = np.zeros(self.params.ngibbs)        # variance
#        self.psigibbs = np.zeros(self.params.ngibbs)        # psi spatial effect intensity
        self.gammagibbs = np.zeros([self.params.ngibbs, len(self.basicdata.gamma)]) # gamma weather
        self.popgibbs = np.zeros([self.params.ngibbs, 4]) 

    def vupdate(self):
        predDiff= self.basicdata.popRast[2:] - self.basicdata.popPred
        predDiff = predDiff * self.basicdata.rastMask[2:]
        sx = np.sum(predDiff*predDiff)
        u1 = self.params.s1 + np.multiply(.5, self.basicdata.ndat)
        u2 = self.params.s2 + np.multiply(.5, sx)
        isg = np.random.gamma(u1, 1/u2, size = None)
        self.basicdata.sigma = 1.0/isg
        self.basicdata.sqrt_sigma = np.sqrt(self.basicdata.sigma)

    def dnorm1SD(self, arr1, mn, sd):
        precalc = 1.0/sd/(np.sqrt(2.0*np.pi))
        denomcalc = 2.0*sd*sd
        diff = (arr1 - mn)
        numcalc = diff*diff
        normpdf = precalc * np.exp(-numcalc/denomcalc)
        return normpdf

    def dbern(self,d, p):
        pp = np.where(d==1, p, 1.0-p)
        return pp

#    def dbern2(self,d, p):
#        pp = (p**d) * ((1-p)**(1-d))
#        return pp


    def popUpdate1910(self):
        """
        Update latent population for 1910
        """
        # Relevant pop rasters
        popRast1912 = self.basicdata.popRast[2]                 # 1912 - target of prediction using proposed pop in 1910
        popRast1910 = self.basicdata.popRast[0]                 # 1910 current value
        popRast1910_s = self.basicdata.popRast_s[0]             # 1910 proposed value
        # population prediction in 1912 using current parameters
        popPred1912 = self.basicdata.popPred[0]  
        # calc new prediction in 1912 using proposed new pop in 1910 (vary DDD only)
        DDD = self.basicdata.delayedDensityDepend[0]                            
        B2 = self.basicdata.B2_rast[0]              
        DDD_s = self.basicdata.calcDelayedDensityDepend(B2, popRast1910_s)
        popPred1912_s = popPred1912 - DDD + DDD_s
        # calc importance ratio of current and proposed pop values in 1910
        norm_pdf = self.dnorm1SD(popRast1912, popPred1912, 
                            self.basicdata.sqrt_sigma)    # probability density
        norm_pdf_s = self.dnorm1SD(popRast1912, popPred1912_s, 
                               self.basicdata.sqrt_sigma)

        pnow = (np.log(norm_pdf) + 
                np.log(self.dnorm1SD(popRast1910, self.params.popPriorMean, 
                        self.params.popPriorSD)))
        pnew = (np.log(norm_pdf_s) + 
                np.log(self.dnorm1SD(popRast1910_s, self.params.popPriorMean, 
                        self.params.popPriorSD)))
        rValue = np.exp(pnew - pnow)        # calc importance ratio (IR)
        # random probability to accept
        zValue = np.random.uniform(self.basicdata.zerosRaster, 
                        self.basicdata.rastMask[0])
        rvMask = rValue > zValue

        # if IR greater that zValue update
        self.basicdata.popRast[0, rvMask] = popRast1910_s[rvMask]
        self.basicdata.delayedDensityDepend[0, rvMask] = DDD_s[rvMask]
        self.basicdata.popPred[0, rvMask] = popPred1912_s[rvMask] 

    def popUpdate1911(self):
        """
        Update latent population for 1911, which influences 1912 and 1913
        """
        # Relevant pop rasters
        popRast1913 = self.basicdata.popRast[3] 
        popRast1912 = self.basicdata.popRast[2]                 # 1912 - target of prediction using proposed pop in 1911
        popRast1911 = self.basicdata.popRast[1]                 # 1911 current value
        popRast1911_s = self.basicdata.popRast_s[1]             # 1911 proposed value
        expPopRast1911_s = self.basicdata.expPopRast_s[1]
        # population prediction in 1912 using current parameters
        popPred1912 = self.basicdata.popPred[0]
        popPred1913 = self.basicdata.popPred[1]
        # calc DD and DDD for 1912 and 1913
        B2_13 = self.basicdata.B2_rast[1]
        B1_12 = self.basicdata.B1_rast[0]              
        DD_s_12 = self.basicdata.calcDensityDepend(B1_12, popRast1911_s)            #DD - 1912
        DD_12 = self.basicdata.densityDepend[0]                                     #DD - 1913
        DDD_13 = self.basicdata.delayedDensityDepend[1]                             #DDD - 1912
        DDD_s_13 = self.basicdata.calcDelayedDensityDepend(B2_13, popRast1911_s)    #DDD - 1913
        # spatial effects for 1912
        SE12_s = rasterutils.calculateSpatialEffect(expPopRast1911_s,
                self.basicdata.rastMask[2], self.basicdata.se_distarray,
                self.params.se_halfwinsize)
        SE12_s = np.log(SE12_s + self.basicdata.logPlus)                                 #* self.basicdata.psi
        SE12 = self.basicdata.spatialEffectRast[0]
        # Predict population size in 1912-13 given proposed pop in 1911
        popPred1912_s = popPred1912 - DD_12 + DD_s_12 - SE12 + SE12_s 
        popPred1913_s = popPred1913 - DDD_13 + DDD_s_13
        # do analysis over masked pixels
        pixMask = self.basicdata.maskBool[1]
        # calc importance ratio of current and proposed pop values in 1911

        normNow = ((np.log(self.dnorm1SD(popRast1912,
                        popPred1912, self.basicdata.sqrt_sigma))) +
                   (np.log(self.dnorm1SD(popRast1913, popPred1913,
                        self.basicdata.sqrt_sigma))))
        priorNow = np.log(self.dnorm1SD(popRast1911, self.params.popPriorMean,
                        self.params.popPriorSD))
        pnowSum = (np.sum(normNow[pixMask]) + np.sum(priorNow[pixMask]))
                
        # pdf data in 1912-13 given proposed pop in 1911
        normNew = ((np.log(self.dnorm1SD(popRast1912,
                        popPred1912_s, self.basicdata.sqrt_sigma))) +
                    (np.log(self.dnorm1SD(popRast1913, popPred1913_s,
                        self.basicdata.sqrt_sigma))))
        priorNew = np.log(self.dnorm1SD(popRast1911_s, self.params.popPriorMean,
                        self.params.popPriorSD))
        pnewSum = (np.sum(normNew[pixMask]) +  np.sum(priorNow[pixMask]))

#        print('pnewSum-pnowSum', pnewSum-pnowSum)
#        print('pnowSum', pnowSum)

        pdiff = pnewSum - pnowSum
        if pdiff > 2:                       # need this because initial parameter are way off
            rValue = 1.0                      # without this step, get LogLik = inf or -inf
            zValue = 0.0
        if pdiff <= 2:
            rValue = np.exp(pdiff)        # calc importance ratio
            zValue = np.random.uniform(0, 1.0, size = None)

        # calc importance ratio (IR)
#        rValue = np.exp(pnewSum - pnowSum)        # calc importance ratio (IR)
        # random probability to accept
#        zValue = np.random.uniform(0.0, 1.0, size = None)
        # if IR greater that zValue update pop, DD, DDD and Spatial effect
        if rValue > zValue:
            self.basicdata.popRast[1] = popRast1911_s
            self.basicdata.expPopRast[1] = expPopRast1911_s
            self.basicdata.densityDepend[0] = DD_s_12
            self.basicdata.popPred[0] = popPred1912_s
            self.basicdata.delayedDensityDepend[1] = DDD_s_13
            self.basicdata.spatialEffectRast[0] = SE12_s
            self.basicdata.popPred[1] = popPred1913_s

    def seFX(self, i):
        se_up1 = rasterutils.calculateSpatialEffect(self.basicdata.expPopRast[i],
                self.basicdata.rastMask[i + 1], self.basicdata.se_distarray,
                self.params.se_halfwinsize)                                 #* self.basicdata.psi
        se = np.log(se_up1 + self.basicdata.logPlus)
        return se


    def popUpdate1912(self):
        """
        update population from 1912 to third to last year (2001)
        """
        # cycle thru years. seqI is arange(2, nyears)
        for i in self.basicdata.seqI:
            # defoliation raster in Time t
            defol_T = self.basicdata.defol[i-2]
            # Relevant pop rasters
            popRast_T = self.basicdata.popRast[i]                  # pop in year t
            popRast_T_s = self.basicdata.popRast_s[i]              # proposed pop at t
            expPopRast_T_s = self.basicdata.expPopRast_s[i] 
            popRast_T_Up1 = self.basicdata.popRast[i+1]
            popRast_T_Up2 = self.basicdata.popRast[i+2]
            # current population predictions
            popPred_T = self.basicdata.popPred[i-2]
            # population prediction in up 1 and 2 years using current parameters
            popPredUp1 = self.basicdata.popPred[i-1]
            popPredUp2 = self.basicdata.popPred[i]
            # calc DD and DDD for up one and two years
            B2_Up2 = self.basicdata.B2_rast[i]
            B1_Up1 = self.basicdata.B1_rast[i-1]              
            DD_s_Up1 = self.basicdata.calcDensityDepend(B1_Up1, popRast_T_s)            #DD
            DD_Up1 = self.basicdata.densityDepend[i-1]                                     #DD - 1913
            DDD_Up2 = self.basicdata.delayedDensityDepend[i]                             #DDD - 1912
            DDD_s_Up2 = self.basicdata.calcDelayedDensityDepend(B2_Up2, popRast_T_s)    #DDD - 1913
            # spatial effects for up one year
            SE_s_Up1 = rasterutils.calculateSpatialEffect(expPopRast_T_s,
                    self.basicdata.rastMask[i-1], self.basicdata.se_distarray,
                    self.params.se_halfwinsize)
            SE_s_Up1 = np.log(SE_s_Up1 + self.basicdata.logPlus)                                 #* self.basicdata.psi
            SE_Up1 = self.basicdata.spatialEffectRast[i-1]
            # Predict population size up 1 and 2 years for new pop values
            popPred_s_Up1 = popPredUp1 - DD_Up1 + DD_s_Up1 - SE_Up1 + SE_s_Up1             
            popPred_s_Up2 = popPredUp2 - DDD_Up2 + DDD_s_Up2 
            # calc normal pdf for T and up 1 and up 2 years
            norm_pdfT = np.log(self.dnorm1SD(popRast_T, 
                                popPred_T, self.basicdata.sqrt_sigma))
            norm_pdfT_s = np.log(self.dnorm1SD(popRast_T_s, 
                                popPred_T, self.basicdata.sqrt_sigma))

            norm_pdfUp1 = np.log(self.dnorm1SD(popRast_T_Up1, 
                                popPredUp1, self.basicdata.sqrt_sigma))
            norm_pdfUp1_s = np.log(self.dnorm1SD(popRast_T_Up1, 
                                popPred_s_Up1, self.basicdata.sqrt_sigma))

            norm_pdfUp2 = np.log(self.dnorm1SD(popRast_T_Up2, 
                                popPredUp2, self.basicdata.sqrt_sigma))
            norm_pdfUp2_s = np.log(self.dnorm1SD(popRast_T_Up2, 
                                popPred_s_Up2, self.basicdata.sqrt_sigma))
            # calc bernouilli density
            defolProb = inv_logit(popRast_T - 5)
            dbernNow = np.log(self.dbern(defol_T, defolProb))
            defolProb_s = inv_logit(popRast_T_s - 5)
            dbernNew = np.log(self.dbern(defol_T, defolProb_s))
            # likelihood
            pnow = mdl.makestack((norm_pdfT, norm_pdfUp1, norm_pdfUp2, dbernNow))
            pnew = mdl.makestack((norm_pdfT_s, norm_pdfUp1_s, norm_pdfUp2_s, dbernNew))
            # mask for current and next two years
            
            maskT12 = self.basicdata.maskBool[i : (i+3)]
            maskT = self.basicdata.maskBool[i]
            tmpMask = mdl.makestack([maskT12, maskT])

#            print('1 - 4', norm_pdfT.shape, norm_pdfUp1.shape, norm_pdfUp2.shape, dbernNow.shape)
#            print('maskT12', maskT12.shape)
#            print('maskT', maskT.shape)
#            print('pnow', pnow.shape, pnew.shape)
#            print('tmpMask', tmpMask.shape)

#            maskT1 = self.basicdata.maskBool[i+1]
#            maskT2 = self.basicdata.maskBool[i+2]
#            tmpMask = np.dstack((maskT12, maskT))
            # sum over masked areas
            tmpNow = np.ma.array(pnow, mask=np.logical_not(tmpMask))
            pnowSum = tmpNow.sum()
            tmpNew = np.ma.array(pnew, mask=np.logical_not(tmpMask))
            pnewSum = tmpNew.sum()
            # calc importance ratio (IR)
            pdiff = pnewSum - pnowSum
            if pdiff > 2:                       # need this because initial parameter are way off
                rValue = 1.0                      # without this step, get LogLik = inf or -inf
                zValue = 0.0
            elif pdiff < -100:
                rValue = 0.0                      # without this step, get LogLik = inf or -inf
                zValue = 1.0
            else:
                rValue = np.exp(pdiff)        # calc importance ratio
                zValue = np.random.uniform(0, 1.0, size = None)
            if rValue > zValue:
                self.basicdata.popRast[i] = popRast_T_s
                self.basicdata.expPopRast[i, ] = expPopRast_T_s
                #update DD and DDD using updated pop in time t
                self.basicdata.densityDepend[i-1] = DD_s_Up1
                self.basicdata.delayedDensityDepend[i] = DDD_s_Up2
                # update spatial effect one year ahead
                self.basicdata.spatialEffectRast[i-1] = SE_s_Up1
                # update popPred up 1 and 2
                self.basicdata.popPred[i-1] = popPred_s_Up1
                self.basicdata.popPred[i] = popPred_s_Up2



    def popUpdateNextToLast(self):
        """
        update next to last population (2002)
        """
        # set i to be second to last year
        i = self.basicdata.nYears
        # defoliation raster in Time t
        defol_T = self.basicdata.defol[i-2]
        # Relevant pop rasters
        popRast_T = self.basicdata.popRast[i]                  # pop in year t
        popRast_T_s = self.basicdata.popRast_s[i]              # proposed pop at t
        expPopRast_T_s = self.basicdata.expPopRast_s[i] 
        popRast_T_Up1 = self.basicdata.popRast[i+1]
        # current population predictions
        popPred_T = self.basicdata.popPred[i-2]
        # population prediction in up 1 and 2 years using current parameters
        popPredUp1 = self.basicdata.popPred[i-1]
        # calc DD and DDD for up one and two years
        B1_Up1 = self.basicdata.B1_rast[i-1]              
        DD_s_Up1 = self.basicdata.calcDensityDepend(B1_Up1, popRast_T_s)            #DD
        DD_Up1 = self.basicdata.densityDepend[i-1]                                     #DD - 1913
        # spatial effects for up one year
        SE_s_Up1 = rasterutils.calculateSpatialEffect(expPopRast_T_s,
                    self.basicdata.rastMask[i-1], self.basicdata.se_distarray,
                    self.params.se_halfwinsize)
        SE_s_Up1 = np.log(SE_s_Up1 + self.basicdata.logPlus)                                 #* self.basicdata.psi
        SE_Up1 = self.basicdata.spatialEffectRast[i-1]
        # Predict population size up 1 and 2 years for new pop values
        popPred_s_Up1 = popPredUp1 - DD_Up1 + DD_s_Up1 - SE_Up1 + SE_s_Up1             
        # calc normal pdf for T and up 1 and up 2 years
        norm_pdfT = np.log(self.dnorm1SD(popRast_T, 
                        popPred_T, self.basicdata.sqrt_sigma))
        norm_pdfT_s = np.log(self.dnorm1SD(popRast_T_s, 
                        popPred_T, self.basicdata.sqrt_sigma))
        norm_pdfUp1 = np.log(self.dnorm1SD(popRast_T_Up1, 
                        popPredUp1, self.basicdata.sqrt_sigma))
        norm_pdfUp1_s = np.log(self.dnorm1SD(popRast_T_Up1, 
                        popPred_s_Up1, self.basicdata.sqrt_sigma))
        # calc bernouilli density
        defolProb = inv_logit(popRast_T - 5)
        dbernNow = np.log(self.dbern(defol_T, defolProb))
        defolProb_s = inv_logit(popRast_T_s - 5)
        dbernNew = np.log(self.dbern(defol_T, defolProb_s))
        # likelihood
        pnow = mdl.makestack((norm_pdfT, norm_pdfUp1, dbernNow))
        pnew = mdl.makestack((norm_pdfT_s, norm_pdfUp1_s, dbernNew))
        # mask for current and next two years
        maskindx = np.array([92, 93, 92])    
        tmpMask = self.basicdata.maskBool[maskindx]
        # sum over masked areas
        tmpNow = np.ma.array(pnow, mask=np.logical_not(tmpMask))
        pnowSum = tmpNow.sum()
        tmpNew = np.ma.array(pnew, mask=np.logical_not(tmpMask))
        pnewSum = tmpNew.sum()
        # calc importance ratio (IR)
        pdiff = pnewSum - pnowSum
        if pdiff > 2:                       # need this because initial parameter are way off
            rValue = 1.0                      # without this step, get LogLik = inf or -inf
            zValue = 0.0
        elif pdiff < -100:
            rValue = 0.0                      # without this step, get LogLik = inf or -inf
            zValue = 1.0
        else:
            rValue = np.exp(pdiff)        # calc importance ratio
            zValue = np.random.uniform(0, 1.0, size = None)
        if rValue > zValue:
            self.basicdata.popRast[i] = popRast_T_s
            self.basicdata.expPopRast[i, ] = expPopRast_T_s
            #update DD and DDD using updated pop in time t
            self.basicdata.densityDepend[i-1] = DD_s_Up1
            # update spatial effect one year ahead
            self.basicdata.spatialEffectRast[i-1] = SE_s_Up1
            # update popPred up 1 and 2
            self.basicdata.popPred[i-1] = popPred_s_Up1


    def popUpdateLastYear(self):
        """
        update population of last year (2003)
        """
        popRast_T = self.basicdata.popRast[-1]       # last year pop
        popRast_T_s = self.basicdata.popRast_s[-1]   # last year proposed value
        # population prediction last year using current parameters
        popPred_T = self.basicdata.popPred[-1]
        # defoliation raster in Time t
        defol_T = self.basicdata.defol[-1]
        # calc importance ratio of current and proposed pop values in 1911
        norm_pdf = np.log(self.dnorm1SD(popRast_T, popPred_T, self.basicdata.sqrt_sigma))
        norm_pdf_s = np.log(self.dnorm1SD(popRast_T_s, popPred_T, self.basicdata.sqrt_sigma)) 
        pnow = norm_pdf + np.log(self.dbern(defol_T, inv_logit(popRast_T - self.params.addLogit)))
        pnew = norm_pdf_s + np.log(self.dbern(defol_T, inv_logit(popRast_T_s - self.params.addLogit)))
        # calc importance ratio (IR)
        rValue = np.exp(pnew - pnow)        # calc importance ratio (IR)
        # random probability to accept
        zValue = np.random.uniform(self.basicdata.zerosRaster, self.basicdata.rastMask[-1])
        # if IR greater that zValue update pop, DD, DDD and Spatial effect
        rvMask = rValue > zValue
        self.basicdata.popRast[-1, rvMask] = popRast_T_s[rvMask]

    def popUpdateAll(self):
        """
        Update latent population for all years
        Call this function in mcmcfx
        """
        self.popUpdate1910()
        self.popUpdate1911()
        self.popUpdate1912()
        self.popUpdateNextToLast()
        self.popUpdateLastYear()
        self.basicdata.proposeNewPop()

    def alphaB0_Update(self):
        """
        Update alpha parameters for calculating B0 (growth rate)
        """
        ## loop through alpha B0 parameters
        tmpMask = self.basicdata.maskBool[2:]
        for i in range(len(self.basicdata.alpha_b0)):
            alpha_b0_s = self.basicdata.alpha_b0.copy()
            # draw new proposal from normal
            alpha_b0_s[i] = np.random.normal(self.basicdata.alpha_b0[i], 0.008, size = None)
            # proposed new B0 raster given proposed alpha parameter
            B0_rast_s = self.basicdata.makeBetaRasters(alpha_b0_s, 
                            self.basicdata.b0_climateVariables)
            popPred_s = (self.basicdata.PopPredict(B0_rast_s,
                           self.basicdata.densityDepend,
                            self.basicdata.delayedDensityDepend,
                            self.basicdata.spatialEffectRast,
                            self.basicdata.weatherEffect))
            
            # calc IR
            # likelihood of population size given predictions times prior on parameters
            dnormPopPred = (np.log(self.dnorm1SD(self.basicdata.popRast[2:],
                            self.basicdata.popPred, self.basicdata.sqrt_sigma)))
            tmp = np.ma.array(dnormPopPred, mask=np.logical_not(tmpMask))
            dnormPopPredSum = tmp.sum()

            priorNow = (np.log(stats.norm.pdf(self.basicdata.alpha_b0[i],
                            self.params.alphaMean, self.params.alphaSD)))
            pnowSum = dnormPopPredSum + priorNow

            dnormPopPred = (np.log(self.dnorm1SD(self.basicdata.popRast[2:],
                            popPred_s, self.basicdata.sqrt_sigma)))
            tmp = np.ma.array(dnormPopPred, mask=np.logical_not(tmpMask))
            dnormPopPredSum = tmp.sum()

            priorNew = (np.log(stats.norm.pdf(alpha_b0_s[i],
                            self.params.alphaMean, self.params.alphaSD)))
            pnewSum = dnormPopPredSum + priorNew    

            pdiff = pnewSum - pnowSum
            if pdiff > 2:                       # need this because initial parameter are way off
                rValue = 1.0                      # without this step, get LogLik = inf or -inf
                zValue = 0.0
            elif pdiff < -100:
                rValue = 0.0                      # without this step, get LogLik = inf or -inf
                zValue = 1.0
            else:
                rValue = np.exp(pdiff)        # calc importance ratio
                zValue = np.random.uniform(0, 1.0, size = None)
            # if proposed parameter is better, keep it and update B0 and popPred
            if rValue > zValue:
                self.basicdata.B0_rast = B0_rast_s.copy()
                self.basicdata.alpha_b0[i] = alpha_b0_s[i]
                self.basicdata.popPred = popPred_s.copy()

#    def pdfwrap(self,arr1, mn, sd):
#        return self.dnorm1SD(arr1, mn, sd)


    def alphaBeta12_Update(self, alphaPara, bRaster, climateRast, P):
        """
        Update alpha parameters for calculating Beta 3-d rasters
        P is indicator for B1 (DD) or B2 (DDD)
        """
        tmpMask = self.basicdata.maskBool[2:]
        ## loop through alpha Beta parameters
        for i in range(len(alphaPara)):
            alphaPara_s = alphaPara.copy()

            if P == 1:
                # draw new proposal from normal
                alphaPara_s[i] = np.random.normal(alphaPara[i], 0.005, size = None)
                # proposed DD effect with new alpha
                # proposed new B0 raster given proposed alpha parameter
                bRaster_s = self.basicdata.makeBetaRasters(alphaPara_s, 
                            climateRast)
                DDEffect_s = (self.basicdata.calcDensityDepend(bRaster_s, 
                                self.basicdata.popRast[1:-1]))

                # predict pop with new alphas
                popPred_s = (self.basicdata.PopPredict(self.basicdata.B0_rast,
                            DDEffect_s,
                            self.basicdata.delayedDensityDepend,
                            self.basicdata.spatialEffectRast,
                            self.basicdata.weatherEffect))

            # population prediction if updating alphas for B2
            if P == 2:
                # draw new proposal from normal
                alphaPara_s[i] = np.random.normal(alphaPara[i], 0.005, size = None)
                # proposed new B0 raster given proposed alpha parameter
                bRaster_s = self.basicdata.makeBetaRasters(alphaPara_s, 
                            climateRast)
                DDDEffect_s = (self.basicdata.calcDelayedDensityDepend(bRaster_s, 
                             self.basicdata.popRast[0:-2]))
                
#                print('DDD now',self.basicdata.delayedDensityDepend[32,20,50])
#                print('DDD new', DDDEffect_s[32,20,50])
#                print('#######################################################')
                

                popPred_s = (self.basicdata.PopPredict(self.basicdata.B0_rast,
                            self.basicdata.densityDepend,
                            DDDEffect_s,
                            self.basicdata.spatialEffectRast,
                            self.basicdata.weatherEffect))
            # calc IR
            # likelihood of population size given predictions times prior on parameters
            dnormPopPredNow = (np.log(self.dnorm1SD(self.basicdata.popRast[2:],
                            self.basicdata.popPred, self.basicdata.sqrt_sigma)))
            # sum only in pixels of interest (in mask)
            tmpNow = np.ma.array(dnormPopPredNow, mask=np.logical_not(tmpMask))
            dnormPopPredSumNow = tmpNow.sum()
            priorNow = (np.log(stats.norm.pdf(alphaPara[i],
                            self.params.alphaMean, self.params.alphaSD)))

#            print('P', P)
#            print('dnormNow and priorNow', dnormPopPredSum, priorNow)

            pnowSum = dnormPopPredSumNow + priorNow

            # evaluate proposed values
            dnormPopPredNew = (np.log(self.dnorm1SD(self.basicdata.popRast[2:],
                            popPred_s, self.basicdata.sqrt_sigma)))
            tmpNew = np.ma.array(dnormPopPredNew, mask=np.logical_not(tmpMask))
            dnormPopPredSumNew = tmpNew.sum()

            priorNew = (np.log(stats.norm.pdf(alphaPara_s[i],
                            self.params.alphaMean, self.params.alphaSD)))
            pnewSum = dnormPopPredSumNew + priorNew

            
#            print('PPPPPPPPPPPP', P)
#            tmp1 = tmpNew - tmpNow
#            tmp2 = np.unravel_index(tmp1.argmin(),tmp1.shape)
#            print('min indx', tmp2)
#            tmp3 = np.unravel_index(tmp1.argmax(),tmp1.shape)
#            print('max indx', tmp3)

#            print('dnorm diff', dnormPopPredNew[30,20,50] - dnormPopPredNow[30,20,50])
#            print('population Diff', popPred_s[30,20,50] - self.basicdata.popPred[30,20,50])
#            print('Diff pnowSum  pnewSum', dnormPopPredSumNew - dnormPopPredSumNow)
#            print('priorNew - priorNow', priorNew - priorNow)
#            print('dnew when now=min(now)', tmpNew[tmpNow == tmpNow.min()], tmpNow.min())
#            print('dnow when new=min(new)', tmpNow[tmpNew == tmpNew.min()], tmpNew.min())
#            print('min dnew and dnow', tmpNew.min(), tmpNow.min())
#            print('##############################################################################')
            

            pdiff = pnewSum - pnowSum

            if pdiff > 2:                       # need this because initial parameter are way off
                rValue = 1.0                      # without this step, get LogLik = inf or -inf
                zValue = 0.0
            elif pdiff < -100:
                rValue = 0.0                      # without this step, get LogLik = inf or -inf
                zValue = 1.0
            else:
                rValue = np.exp(pdiff)        # calc importance ratio
                zValue = np.random.uniform(0, 1.0, size = None)

            # if proposed parameter is better, keep it and update B0 and popPred
            if rValue > zValue:
                alphaPara[i] = alphaPara_s[i]
                self.basicdata.popPred = popPred_s.copy()
                # update only if B1
                if P == 1:
                    self.basicdata.B1_rast = bRaster_s.copy()
                    self.basicdata.alpha_b1[i] = alphaPara_s[i]
                    self.basicdata.densityDepend = DDEffect_s.copy()
                # update only if B2
                if P == 2:
                    self.basicdata.B2_rast = bRaster_s.copy()
                    self.basicdata.alpha_b2[i] = alphaPara_s[i]
                    self.basicdata.delayedDensityDepend = DDDEffect_s.copy()
        
#        poprast = self.basicdata.popRast[15]
#        predrastNow = self.basicdata.popPred[13]
#        predrastNew = popPred_s[13]
#        dnow = tmpNow[13]
#        dnew = tmpNew[13]
#        tmpMask = self.basicdata.rastMask[15]
#        tmpdefol = self.basicdata.defol[13]
#        rasterutils.writeToFile(poprast, 'poprast.img')
#        rasterutils.writeToFile(predrastNow, 'predrastnow.img')
#        rasterutils.writeToFile(predrastNew, 'predrastnew.img')
#        rasterutils.writeToFile(dnow, 'dnow.img')
#        rasterutils.writeToFile(dnew, 'dnew.img')
#        rasterutils.writeToFile(tmpdefol, 'defol.img')
#        rasterutils.writeToFile(tmpMask, 'mask.img')
            


    def k_Update(self):
        """
        Update k parameters for calculating spatial effects
        """
        tmpMask = self.basicdata.maskBool[2:]
        k_s = np.exp(np.random.normal(np.log(self.basicdata.k), 0.0075, size = None))
        distarray_s = rasterutils.makeDistArray(k_s,
                                self.params.se_winsize, self.params.se_halfwinsize)        
        # 3-d array of PROPOSED spatial effects
        spatialEffectRast_s = self.basicdata.spatialEffectFX(distarray_s)


#        distarr = rasterutils.makeDistArray(self.basicdata.k,
#                                self.params.se_winsize, self.params.se_halfwinsize)
        # 3-d array of PROPOSED spatial effects
#        se = self.basicdata.spatialEffectFX(distarr)


#        spatialEffectPsi_s = spatialEffectRast_s * self.basicdata.psi
        # predict pop with new k value
        popPred_s = self.basicdata.PopPredict(self.basicdata.B0_rast,
                            self.basicdata.densityDepend,
                            self.basicdata.delayedDensityDepend,
                            spatialEffectRast_s,
                            self.basicdata.weatherEffect)

        dnormPopPred = (np.log(self.dnorm1SD(self.basicdata.popRast[2:],
                            self.basicdata.popPred, self.basicdata.sqrt_sigma)))
        tmp = np.ma.array(dnormPopPred, mask=np.logical_not(tmpMask))
        dnormPopPredSum = tmp.sum()
        priorNow = (np.log(stats.norm.pdf(np.log(self.basicdata.k),
                            self.params.kPriorMean, self.params.kPriorSD)))
        pnowSum = dnormPopPredSum + priorNow
        # evaluate proposed values
        dnormPopPred_s = (np.log(self.dnorm1SD(self.basicdata.popRast[2:],
                            popPred_s, self.basicdata.sqrt_sigma)))
        tmp = np.ma.array(dnormPopPred_s, mask=np.logical_not(tmpMask))
        dnormPopPredSum_s = tmp.sum()
        priorNew = (np.log(stats.norm.pdf(np.log(k_s),
                            self.params.kPriorMean, self.params.kPriorSD)))
        pnewSum = dnormPopPredSum_s + priorNew

        pdiff = pnewSum - pnowSum
        if pdiff > 2:                       # need this because initial parameter are way off
            rValue = 1.0                      # without this step, get LogLik = inf or -inf
            zValue = 0.0
        elif pdiff < -100:
            rValue = 0.0                      # without this step, get LogLik = inf or -inf
            zValue = 1.0
        else:
            rValue = np.exp(pdiff)        # calc importance ratio
            zValue = np.random.uniform(0, 1.0, size = None)

#        print('se', self.basicdata.spatialEffectRast[18, 38:42, 103:107])

#        print('distarr', distarr)
#        print('self.basicdata.distarray', self.basicdata.se_distarray)
#        print('se', se[18, 38:42, 103:107])
#        print('self.basicdata.se', self.basicdata.spatialEffectRast[18, 38:42, 103:107])

#        print('se_s', spatialEffectRast_s[18, 38:42, 103:107]) 

#        print('k_s', k_s)
#        print('r-z', rValue - zValue)
#        print('dnorm and dnorm_s', dnormPopPredSum , dnormPopPredSum_s)
#        print('priornow and priornew', priorNow, priorNew)

        # if proposed parameter is better, keep it and update 
        if rValue > zValue:
            self.basicdata.k = k_s.copy()   
            self.basicdata.spatialEffectRast = spatialEffectRast_s.copy()
#            self.basicdata.spatialEffectPsi = spatialEffectPsi_s.copy() 
            self.basicdata.popPred = popPred_s.copy()
            self.basicdata.se_distarray = distarray_s.copy()

###############
########
##################################   Removed psi Update
#####
########
##############

    def gamma_Update(self):
        """
        Update gamma parameters for calculating Weather effect rasters
        """
        tmpMask = self.basicdata.maskBool[2:]
        ## loop through gamm parameters
        npara = len(self.basicdata.gamma)
        for i in range(npara):
            gamma_s = self.basicdata.gamma.copy()
            # draw new proposal from normal
            gamma_s[i] = np.random.normal(self.basicdata.gamma[i], 0.01)
            weatherGamma_s = self.basicdata.weatherGamma.copy()
            # proposed weather gamma 3-d array for gamma[i]
            weatherGamma_s[i] = gamma_s[i] * self.basicdata.weatherVariables[i]
            # get overall weather effect with gamma_s[i] - sum across variables
            weatherEffect_s = self.basicdata.calcWeatherEffect(weatherGamma_s)
            # pred population with gamma_s and weather effects
            popPred_s = (self.basicdata.PopPredict(self.basicdata.B0_rast,
                            self.basicdata.densityDepend,
                            self.basicdata.delayedDensityDepend,
                            self.basicdata.spatialEffectRast,
                            weatherEffect_s))
            dnormPopPred = (np.log(self.dnorm1SD(self.basicdata.popRast[2:],
                            self.basicdata.popPred, self.basicdata.sqrt_sigma)))
            tmp = np.ma.array(dnormPopPred, mask=np.logical_not(tmpMask))
            dnormPopPredSum = tmp.sum()
            priorNow = (np.log(stats.norm.pdf(self.basicdata.gamma[i],
                            self.params.gammaPriorMean, self.params.gammaPriorSD)))
            pnowSum = dnormPopPredSum + priorNow

            # evaluate proposed values
            dnormPopPred = (np.log(self.dnorm1SD(self.basicdata.popRast[2:],
                            popPred_s, self.basicdata.sqrt_sigma)))
            tmp = np.ma.array(dnormPopPred, mask=np.logical_not(tmpMask))
            dnormPopPredSum = tmp.sum()
            priorNew = (np.log(stats.norm.pdf(gamma_s[i],
                            self.params.gammaPriorMean, self.params.gammaPriorSD)))
            pnewSum = dnormPopPredSum + priorNew

            pdiff = pnewSum - pnowSum
            if pdiff > 2:                       # need this because initial parameter are way off
                rValue = 1.0                      # without this step, get LogLik = inf or -inf
                zValue = 0.0
            elif pdiff < -100:
                rValue = 0.0                      # without this step, get LogLik = inf or -inf
                zValue = 1.0
            else:
                rValue = np.exp(pdiff)        # calc importance ratio
                zValue = np.random.uniform(0, 1.0, size = None)

            # if proposed parameter is better, keep it and update B0 and popPred
            if rValue > zValue:
                self.basicdata.gamma[i] = gamma_s[i].copy()
                self.basicdata.weatherGamma[i] = weatherGamma_s[i]
                self.basicdata.weatherEffect = weatherEffect_s.copy()
                self.basicdata.popPred = popPred_s.copy()


########            Main mcmc function
########
    def mcmcFX(self):
        cc = 0
        for g in range(self.params.ngibbs * self.params.thinrate + self.params.burnin):

            # update sigma
            self.vupdate()
#            self.supdate()

            self.popUpdateAll()
            self.alphaB0_Update()

            # updata alphas for Beta1
            self.alphaBeta12_Update(self.basicdata.alpha_b1, self.basicdata.B1_rast, 
                                     self.basicdata.b1_climateVariables, 1)
            # updata alphas for Beta2
            self.alphaBeta12_Update(self.basicdata.alpha_b2, self.basicdata.B2_rast, 
                                     self.basicdata.b2_climateVariables, 2)

            # update k parameter
            self.k_Update()

            # update psi
#            self.psi_Update()

            self.gamma_Update()
            
            if g in self.params.keepseq:
                self.B0gibbs[cc] = self.basicdata.alpha_b0
                self.B1gibbs[cc] = self.basicdata.alpha_b1
                self.B2gibbs[cc] = self.basicdata.alpha_b2
                self.kgibbs[cc] = self.basicdata.k
#                self.psigibbs[cc] = self.basicdata.psi
                self.gammagibbs[cc] = self.basicdata.gamma
                self.siggibbs[cc] = self.basicdata.sigma
                popG = np.array([self.basicdata.popRast[34, 20, 50],
                                self.basicdata.popPred[32, 20, 50],
                                self.basicdata.popRast[20, 40, 105],
                                self.basicdata.popPred[18, 40, 105]])
                self.popgibbs[cc] = popG.copy()

                cc = cc + 1


########            Pickle results to directory
########




class Gibbs(object):
    def __init__(self, mcmcobj):
        self.B0gibbs = mcmcobj.B0gibbs
        self.B1gibbs = mcmcobj.B1gibbs
        self.B2gibbs = mcmcobj.B2gibbs
        self.kgibbs = mcmcobj.kgibbs
#        self.psigibbs = mcmcobj.psigibbs
        self.gammagibbs = mcmcobj.gammagibbs
        self.siggibbs = mcmcobj.siggibbs
        self.popgibbs = mcmcobj.popgibbs

########            Main function
#######
def main(params, basicdatapickle=None, poprastpickle=None):

    #np.seterr(all='raise')

    defolpath = os.getenv('DEFOLPROJDIR', default='.')
    # paths and data to read in
    defolWeathDatFile = os.path.join(defolpath,'defolWeath.csv')
    climateDatFile = os.path.join(defolpath,'climate.csv')

    # initiate basicdata class and object when do not read in previous results
    if basicdatapickle is None:
        basicdata = BasicData(defolWeathDatFile, climateDatFile, params)

    else:
        # read in pickled results (basicdata) from a previous run
#        inputBasicdata = os.path.join(defolpath, picklefile)
        fileobj = open(basicdatapickle, 'rb')
        basicdata = pickle.load(fileobj)
        fileobj.close()

    if poprastpickle is not None:
        fileobj = open(poprastpickle, 'rb')
        basicdata.popRast = pickle.load(fileobj)
        fileobj.close()


    # initiate mcmc class and object
    mcmcobj = MCMC(params, basicdata)

    # run mcmcFX - gibbs loop
    mcmcobj.mcmcFX()

    gibbsobj = Gibbs(mcmcobj)

    # pickle basic data from present run to be used to initiate new runs
    outBasicdata = os.path.join(defolpath,'out_basicdata.pkl')
    fileobj = open(outBasicdata, 'wb')
    pickle.dump(basicdata, fileobj)
    fileobj.close()

    # pickle mcmc results for post processing in gibbsProcessing.py
    outGibbs = os.path.join(defolpath,'out_gibbs.pkl')
    fileobj = open(outGibbs, 'wb')
    pickle.dump(gibbsobj, fileobj)
    fileobj.close()

    # pickle basicdata.popRast
    population = basicdata.popRast
    outPop = os.path.join(defolpath,'out_population.pkl')
    fileobj = open(outPop, 'wb')
    pickle.dump(population, fileobj)
    fileobj.close()


if __name__ == '__main__':
    main()


