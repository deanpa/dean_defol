#!/usr/bin/env python  

#import sys
#import defol
import numpy as np 
import rasterutils

class Params(object):
    def __init__(self):
        """
        Object to set initial parameters
        """

        ##############
        ###############      Initial values parameters for updating
        # k is decay parameter for spatial effect
        self.k = 1.0

        # neighbourhood of spatial effect in pixels 6*10 = 60 km radius
        self.se_winsize = 11
        self.se_halfwinsize = int(self.se_winsize / 2)
        self.resol = 10000  # in metres

        # make dist array for neighbourhood window incorporating k
        self.se_distarray = rasterutils.makeDistArray(self.k,
                                self.se_winsize, self.se_halfwinsize)
        # alphas for b0 - intrinsic rate of growth
        self.alpha_b0 = np.array([3.02, .01, -0.01, 0.0002])                 # intercept and ax1 only
        # alphas for b1 - density dependence
        self.alpha_b1 = np.array([.10, .01, .013, 0.0001])             # intercept, ax1, and ax2
        # alphas for b2 - delayed density dependence
        self.alpha_b2 = np.array([-.024, .001, -.01, -0.0001])
    
        # beta12 search window
        self.searchBeta = ([0.13, 0.13])

        # sigma parameters for B1 and B2
        self.sigmaBeta12 = np.array([.1, .1])
        # priors on sigmaB12
#        self.s1 = 0.1
#        self.s2 = 0.1

        # gamma parameter for weather variables
        self.gamma = np.array([0.039, -.01])
#        # psi parameter for spatial effect
#        self.psi = .005
        # sigma variance parameter of population size
        self.sigma = 1.5
        self.sqrt_sigma = np.sqrt(self.sigma)
        
        ###########################################
        # parameters on probability of detection - observation model
        # delta - mean prob of detection
        self.delta0 = -1.0
        self.delta0Priors = np.array([0.0, 4.0])         # normal priors on delta0
        # rho intensity of probability of detection
#        self.rho = 1.0                                         # specify in defol.py
#        self.rhoPriors = np.array([0, 2])            # normal priors on rho
        # variance on detection - error
#        self.sigmaDetect = .35
#        self.priorSigmaDetect = np.array([0.1, 0.1])   # gamma priors on detection-error variance
        self.nYearsChange = 25
        self.logPlus = 0.005
#        self.addLogit = 5.0

        ######################################
        # modify climate variables used to calculate beta values
        # 0 is ax1 only
        # 1 is ax2 only
        # 0,1 is ax1 and ax2

        self.climateDictionary = {'ax1' : 0, 'ax2' : 1, 'SpFir' : 2}
        self.ax1 = self.climateDictionary['ax1']
        self.ax2 = self.climateDictionary['ax2']
        self.pCovSpFir = self.climateDictionary['SpFir']

        self.b0_climateIndx = np.array([self.ax1, self.ax2, self.pCovSpFir], dtype = int)        # ax1
        self.b1_climateIndx = np.array([self.ax1, self.ax2, self.pCovSpFir], dtype = int)     # ax1 and ax2
        self.b2_climateIndx = np.array([self.ax1, self.ax2, self.pCovSpFir], dtype = int)        # ax1

        ## Priors for alpha parameters (normal distribution)
        self.alphaMean = 0.0                                # mean
        self.alphaSD = np.sqrt(1.0)                        # standard dev
        self.alphaGrowthSD = 2.5
        ######################################


        ######################################
        # modify weather Effect variables used
        # 0 is  minWinTemp
        # 1 is minSumTemp
        # 2 is pcpJune

        self.weatherDictionary = {'minWinTemp' : 0, 'minSumTemp' : 1, 'pcpJune' : 2}
        self.minWinTemp = self.weatherDictionary['minWinTemp']
        self.pcpJune = self.weatherDictionary['pcpJune']

        self.WeatherIndx = np.array([self.minWinTemp, self.pcpJune], dtype = int)

        # gamma parameter for weather variables
#        self.gamma = np.array([0.01])                                        # use only min winter for now
        # gamma priors
        self.gammaPriorMean = 0.0
        self.gammaPriorSD = np.sqrt(1.0)
        ######################################

        # psi parameter for spatial effect
#        self.psi = .005
        # psi priors
#        self.psiPriorMean = 0.0
#        self.psiPriorSD = np.sqrt(1000)

        # sigma variance parameter of population size
#        self.sigma = 1.0
        # priors on sigma
        self.s1 = 0.001 # 3 #6 #1 #0.1
        self.s2 = 0.001 # 1000.0    # 3 #15    #.1 #0.1

        # Priors on pop size in 1910 and 1911
        self.popPriorMean = np.array([1])
        self.popPriorSD = np.array([4.])

        # k parameter priors
        self.kPriorMean = np.log(1)
        self.kPriorSD = np.sqrt(1)

        # search variance parameter for proposing new pop values
        # see function proposeNewPop
        self.searchPopVar = 0.025

        ###################################################
        ###################################################
        # Set number of MCMC iterations, thin rate and burn in
        self.ngibbs = 250    #250        # number of estimates to save for each parameter
        self.thinrate = 1000   # thin rate
        self.burnin = 100     #burn in number of iterations
        # array of iteration id to keep
        self.keepseq = np.arange(self.burnin, ((self.ngibbs * self.thinrate) + self.burnin),
            self.thinrate)
        ###################################################
        ###################################################



