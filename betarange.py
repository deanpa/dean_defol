#!/usr/bin/env python

import os
import numpy as np
from numba import autojit
import pickle
import pylab as P
from defol import BetaParameters
import params

class GenPara(object):
    def __init__(self, params):
        """
        Object to parameter space for DD and DDD
        """

        self.params = params
#        self.searchBeta = np.array([0.03, .01])        
#        self.searchBeta = self.params.searchBeta
        ## set period bounds
        self.minPeriod = 16.0
        self.maxPeriod = 44.0

        self.values = 1300
        self.B1_seq = np.linspace(.03, 2.0, num = self.values)     #np.arange(-.3, .3, step=.01)
        self.B2_seq = np.linspace(-.99999, -0.00044, num = self.values)
        self.variables = 2

#beta min [ 0.04180915 -0.99999   ]
#beta max [  1.97732881e+00  -5.00000000e-04]

        #############################
        # run functions
        ######

        self.gendata()
        self.filterB1()
        self.getB1B2()
        self.probJumpB12()
        self.plotFX()
 
        ######
        ############################

    def gendata(self):
        """
        gen coeff for beta1 (DD) and (DDD)
        """
        self.ntot = np.int(self.values**self.variables)
        self.indxDat = np.zeros(self.ntot, dtype = int)
        print('ntot', self.ntot)
        self.periodBeta = np.empty((self.ntot, 2))
        self.periodBeta[:,0] = np.repeat(self.B1_seq, self.values)
        self.periodBeta[:,1] = np.tile(self.B2_seq, self.values)

    def filterB1(self):
        """
        filter out betas
        """
        arg1 = np.sqrt(-4 * self.periodBeta[:,1])
#        print('len arg periodBeta', len(arg1), np.shape(self.periodBeta))
#        print('arg', arg1[0:100])
     
        filterMask = self.periodBeta[:,0] < arg1
        self.periodBeta = self.periodBeta[filterMask]
#        print(' periodBeta', np.shape(self.periodBeta))
#        print('periodbeta shp', np.shape(self.periodBeta)) 

    def calcPeriod(self):
        """
        Calc periodicity 
        """
        numerator = self.periodBeta[:,0] / 2.0 / np.sqrt(-self.periodBeta[:,1])
        print(len(numerator), np.shape(self.periodBeta))
        self.period = 2.0 * np.pi / np.arccos(numerator)

#    @autojit
    def getB1B2(self):
        """
        Get beta parameters for DD and DDD
        """
        self.calcPeriod()
        maskLow = self.period >= self.minPeriod
        maskHigh = self.period <= self.maxPeriod
        maskPeriod = maskLow & maskHigh

        self.periodBeta = self.periodBeta[maskPeriod]
        self.period = self.period[maskPeriod]

        print('periodBeta shp', np.shape(self.periodBeta))
        print('beta min', np.min(self.periodBeta, axis = 0))
        print('beta max', np.max(self.periodBeta, axis = 0))
        print('min max period', np.min(self.period), np.max(self.period))
        print('unique b1', len(np.unique(self.periodBeta[:,0])))
        print('unique b2', len(np.unique(self.periodBeta[:,1])))
        print('beta12', self.periodBeta[0:20])

    def probJumpB12(self):
        """
        calc prob of jumping to another Beta value given a specified beta
        for metropolis hastings
        """
        print('searchBeta', self.params.searchBeta)
        nperiodDat = len(self.period)
        self.jumpProbB12 = np.empty(nperiodDat)
        # get range of possible proposal
        b1_s = self.periodBeta[:, 0]
        b2_s = self.periodBeta[:, 1]
        keepBeta = np.ones(nperiodDat)
        tt = 0
        # cycle through periodBeta data
        for i in range(nperiodDat):
            keepBeta[:] = 1
            # restrict possible proposals
            b1Diff = np.abs(b1_s[i] - b1_s)
            b2Diff = np.abs(b2_s[i] - b2_s)
            keepBeta[b1Diff > self.params.searchBeta[0]] = 0
            keepBeta[b2Diff > self.params.searchBeta[1]] = 0
            keepBeta[i] = 0
            keepBeta[i] = 0
            # calc log probability of getting present betas given the proposal (b1_s and b2_s)
            sumkb = np.sum(keepBeta)
            if sumkb == 1:
                tt += 1
#                print('iiiiii', i)
                print('b1 and b2', b1_s[i], b2_s[i])
            self.jumpProbB12[i] = (1 / sumkb)
        self.jumpProbB12 = np.log(self.jumpProbB12)
        print('min, mean, max jumpProbB12', np.min(self.jumpProbB12), np.mean(self.jumpProbB12), np.max(self.jumpProbB12))
        print('tt', tt)


    def plotFX(self):
        """
        plot hist of periods, and betas
        """
        P.figure()
        P.subplot(2,3,1)
        P.hist(self.period)

        P.subplot(2,3,2)
        P.scatter(self.periodBeta[:,0], self.periodBeta[:,1])

        P.subplot(2,3,3)
        P.hist(self.periodBeta[:,0])

        P.subplot(2,3,4)
        P.hist(self.periodBeta[:,1])

        P.subplot(2,3,5)
        P.hist(self.jumpProbB12)        
        P.show()


########            Main function
#######
def main(params):

    defolpath = os.getenv('DEFOLPROJDIR', default='.')
    # paths and data to read in
#    climateDatFile = os.path.join(defolpath,'climate.csv')

    genpara = GenPara(params)

    betaparameters = BetaParameters(genpara)

    # pickle basic data from present run to be used to initiate new runs
#    outBetaParameters = os.path.join(defolpath,'out_betaparameters.pkl')
#    fileobj = open(outBetaParameters, 'wb')
#    pickle.dump(betaparameters, fileobj)
#    fileobj.close()


if __name__ == '__main__':
    p = params.Params()
    main(p)

