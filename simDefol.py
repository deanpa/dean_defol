#!/usr/bin/env python

import params
import os
import numpy as np
import pylab as P
from scipy.stats.mstats import mquantiles
import prettytable
import pickle
import rasterutils

def logit(x):
    """
    Function (logit) to convert probability to real number
    """
    return np.log(x) - np.log(1 - x)

def inv_logit(x):
    """
    Function to convert real number to probability
    """
    return np.exp(x) / (1 + np.exp(x))



class ResultsProcessing(object):
    def __init__(self, gibbsobj, basicdata, params):

        self.basicdata = basicdata
        self.gibbsobj = gibbsobj
        self.ndat = len(self.gibbsobj.siggibbs)

##############################
        self.params = params
        self.se_winsize = self.params.se_winsize
        self.se_halfwinsize = int(self.se_winsize / 2)
        self.logPlus = self.params.logPlus

##############################


        self.simCol = np.array([39, 14, 84, 94])
        self.simRow = np.array([40, 13, 53, 10])
        self.npix = len(self.simRow)
        self.nPops = self.basicdata.popRast.shape[0]
        self.ngibbs = len(self.gibbsobj.kgibbs)
        self.randID = np.random.permutation(self.ngibbs)[:self.basicdata.nYears]
        self.b0_a = self.gibbsobj.B0gibbs[self.randID]      
        self.b1_a = self.gibbsobj.B1gibbs[self.randID]
        self.b2_a = self.gibbsobj.B2gibbs[self.randID]
        self.kk = self.gibbsobj.kgibbs[self.randID]
        self.gg = self.gibbsobj.gammagibbs[self.randID]
        self.ss = self.gibbsobj.siggibbs[self.randID]
#        self.ss = np.sqrt(self.ss)

        # get mean alpha values for B0, B1, and B2 for calculation of period
        self.a_meanB0 = np.round(np.mean(self.gibbsobj.B0gibbs, axis = 0), 5)
        self.a_meanB1 = np.round(np.mean(self.gibbsobj.B1gibbs, axis = 0), 5)
        self.a_meanB2 = np.round(np.mean(self.gibbsobj.B2gibbs, axis = 0), 5)

        # make 3-d arrays of Betas using the alphas randomly selected
        # use to simulate populations

        print('shp bo_a', self.b0_a.shape, 'shp b0_climate', self.basicdata.b0_climateVariables.shape)

        self.B0_rast = self.makeBetaRasters(self.b0_a, self.basicdata.b0_climateVariables)  # rmax
        self.B1_rast = self.makeBetaRasters(self.b1_a, self.basicdata.b1_climateVariables)  # DD
        self.B2_rast = self.makeBetaRasters(self.b2_a, self.basicdata.b2_climateVariables)  # DDD

        # get B0, B1 and B2 for the pixels for calculation of period
        # however, should include a spatial effect
        self.B0 = self.pixelBetas(self.a_meanB0, self.basicdata.b0_climateVariables)
        self.B1 = self.pixelBetas(self.a_meanB1, self.basicdata.b1_climateVariables)
        self.B2 = self.pixelBetas(self.a_meanB2, self.basicdata.b2_climateVariables)

        print('mean b0', self.B0)
        print('mean b1', self.B1)
        print('mean b2', self.B2)
        
        # calculate periodicity
        self.calcPeriod()
        print('Period', self.period)

        # calc weather effect using weather variables and random gamma from mcmc
        self.calcWeatherGamma()
        self.weatherEffect = self.calcWeatherEffect()

        # simulate populations over all years
###        self.simPop()

#        print('prob defol', self.probDefol[:, self.simRow[1], self.simCol[1]])
#        print('simPop', self.simPop[:, self.simRow[1], self.simCol[1]])
        
###        print('climat Var Bo', self.basicdata.b0_climateVariables[0, 0:5, self.simRow, self.simCol])
###        print('climat Var Bo', self.basicdata.B0_rast[0:5, self.simRow, self.simCol])

####################################################
############
##                  Functions
###########
####################################################
    def makeBetaRasters(self, alpha, climateVariables):
        """
        make 3-d rasters for B0, B1, and B2
        use randomly selected alphas from mcmc
        """
        # Cycle thru climate 3-d arrays for Betas to get Beta_raster
        Beta_rast = np.zeros((self.basicdata.nYears, rasterutils.N_YPIX, rasterutils.N_XPIX))
        for i in range(np.shape(alpha)[1] - 1):
            # expand dims 2x to multiply one alpha for each year times yearly climate raster 
            ai1 = np.expand_dims(alpha[:, i + 1], 1) 
            ai2 = np.expand_dims(ai1, 1) 
            print('shp ai1', ai1.shape, 'shp ai2', ai2.shape)
            np.add(Beta_rast, (ai2 * climateVariables[i]), out = Beta_rast)
#            print('shp alpha, ai1, and ai2 ###################')
#            print(alpha.shape, ai1.shape, ai2.shape)
        # Add intercept alpha value
        a01 = np.expand_dims(alpha[:,0], 1)
        a02 = np.expand_dims(a01, 1)
        np.add(Beta_rast, a02, out = Beta_rast)
        return (Beta_rast)

    def pixelBetas(self, alpha, climateVar):
        """
        get b0, b1, and b2 for the specified pixels and calc outbreak period
        """
        #beta 3-d rasters for r, dd, and ddd
        Beta_Val = np.zeros(4) # np.zeros(rasterutils.N_YPIX, rasterutils.N_XPIX)
        for i in range(len(alpha) - 1):
            climateVal = climateVar[i,90, self.simRow, self.simCol]
            np.add(Beta_Val, (alpha[i+1] * climateVal), out = Beta_Val)
        # Add intercept alpha value
        np.add(Beta_Val, alpha[0], out = Beta_Val)
        # get betas for specified
        return (Beta_Val)

################
###############

    def makeBeta0Raster(self, alpha, climateVariables):
        """
        make 3-d rasters for B0
        Betas in equation 5
        Use to update alpha parameters.
        """
        # Cycle thru climate 3-d arrays for B0 to get B0_raster
        Beta_rast = np.zeros((self.nYears, rasterutils.N_YPIX, rasterutils.N_XPIX))
        for i in range(len(alpha) - 1):
            np.add(Beta_rast, (alpha[i + 1] * climateVariables[i]), out = Beta_rast)
        # Add intercept alpha value
        np.add(Beta_rast, alpha[0], out = Beta_rast)
        return (Beta_rast)

    def makeBeta12_Mu(self, alpha, climateVariables):
        """
        make 2-d rasters for B1 and B2
        Betas in equation 5
        """
        # Cycle thru climate 3-d arrays for B0 to get B0_raster
        Beta_rast = np.zeros((rasterutils.N_YPIX, rasterutils.N_XPIX))
        for i in range(len(alpha) - 1):
            np.add(Beta_rast, (alpha[i + 1] * climateVariables[i]), out = Beta_rast)
        # Add intercept alpha value
        np.add(Beta_rast, alpha[0], out = Beta_rast)
        return (Beta_rast)



    def makeInitialB12Rasters(self, centreValue, searchWindow):
        """
        make 3-d rasters for  B1, and B2
        Betas in equation 5
        """
        #  create B1 and B2 rasters: same values across years
        minV = centreValue - searchWindow
        maxV = centreValue + searchWindow
        maskLow = self.period >= minV
        maskHigh = self.period <= maxV
        periodMask = maskLow & maskHigh
        periodBetaIndxPool = self.periodIndx[periodMask]
        nCellsInYear = rasterutils.N_YPIX * rasterutils.N_XPIX
        selBetas = np.random.choice(periodBetaIndxPool, nCellsInYear, replace = True)
        b1tmp = self.periodBeta[selBetas, 0]
        b1tmp = b1tmp.reshape(rasterutils.N_YPIX, rasterutils.N_XPIX)
        b2tmp = self.periodBeta[selBetas, 1]
        b2tmp = b2tmp.reshape(rasterutils.N_YPIX, rasterutils.N_XPIX)
        self.B1_rast = np.empty((self.nYears, rasterutils.N_YPIX, rasterutils.N_XPIX))
        self.B2_rast = np.empty((self.nYears, rasterutils.N_YPIX, rasterutils.N_XPIX))
        self.B1_rast[0:self.nYears] = b1tmp
        self.B2_rast[0:self.nYears] = b2tmp
        # arrays used in B12 update
        self.B1_rast_s = np.zeros_like(self.B1_rast)
        self.B2_rast_s = np.zeros_like(self.B1_rast)
#        # make dictionary to index appropriate beta raster in B12 update
#        self.betaRastDict = {'beta_rast1' : self.B1_rast[70] , 'beta_rast2' : self.B2_rast[70]}
        # a single year mask that includes both Ont and Minn
#        self.betaMask = self.rastMask[72]
        self.xsize = rasterutils.N_XPIX
        self.ysize = rasterutils.N_YPIX
        # rasters for jump probabilities in B12 update
        self.jumpB12 = np.ones((self.ysize, self.xsize))
        self.jumpB12_s = np.ones((self.ysize, self.xsize))



###########################
############################


    def calcPeriod(self):
        """
        Calc periodicity for four cells.
        """
        numerator = self.B1/2.0/np.sqrt(-self.B2)
        self.period = 2.0*np.pi/np.arccos(numerator)


    def calcWeatherGamma(self):
        """
        multiply gamma times weather variable and keep as 3 or 4-d array
        """
        self.weatherGamma = self.basicdata.weatherVariables.copy()
        for i in range(len(self.basicdata.gamma)):
            # need to expand dims for multiplication
            gi1 = np.expand_dims(self.gg[:,0],1)
            gi2 = np.expand_dims(gi1, 1)
            self.weatherGamma[i] = gi2 * self.basicdata.weatherVariables[i]

    def calcWeatherEffect(self):
        """
        Calc 3-d weather effect using variables and gamma parameters
        """
        return np.sum(self.weatherGamma, axis = 0)


    def calcSpatialEffect(self, i):
        """
        calc spatial effect for year i
        """
        # distance array with parameter k for year i
        distarray = rasterutils.makeDistArray(self.kk[i],
            self.se_winsize, self.se_halfwinsize)
        # spatial effect for year i
        spatialEffecti = rasterutils.calculateSpatialEffect(self.expSimPop[i+1],
            self.basicdata.rastMask[i+2], distarray, self.se_halfwinsize,
            self.logPlus)
        # take log of spatial effect
        spatialEffecti = np.log(spatialEffecti + self.params.logPlus)
        return spatialEffecti


    def PopPredict(self, B0, DD, DDD, SE, WE):
        """
        calc population prediction a given  year and cells
        """
        tmpPopPred = B0 + DD + DDD + SE + WE
        return(tmpPopPred)


    def simPop(self):
        """
        Function to 3-d array of sim pop
        """
        self.simPop = np.zeros(((self.basicdata.nYears + 2), rasterutils.N_YPIX, rasterutils.N_XPIX))
        self.simPop[0:2] = self.basicdata.popRast[0:2]
        self.expSimPop = self.simPop.copy()
        self.expSimPop[0:2] = np.exp(self.simPop[0:2]) * self.basicdata.rastMask[0:2] 
        # rast of probabilities of defoliation
        self.probDefol = np.zeros((self.basicdata.nYears, rasterutils.N_YPIX, rasterutils.N_XPIX))
        for i in range(self.basicdata.nYears):
            popGrowth = self.B0_rast[i]                     # pop growth for year i
            DDi = self.B1_rast[i] * self.simPop[i + 1]      # DD for year i
            DDDi = self.B2_rast[i] * self.simPop[i]         # DDD for year i
            # spatial effect for year i
            SE = self.calcSpatialEffect(i) 
            WE = self.weatherEffect[i]
            popPred = self.PopPredict(popGrowth, DDi, DDDi, SE, WE)
            popPred = np.random.normal(popPred, self.ss[i])
            popPred = popPred * self.basicdata.rastMask[i+2]
            self.simPop[i+2] = popPred
            self.expSimPop[i+2] = np.exp(popPred) * self.basicdata.rastMask[i+2]
            self.probDefol[i] = inv_logit(popPred - self.addLogit)
            
            if i<10:
                print('SE', SE[self.simRow, self.simCol])


    def plotFX(self):
        nfigures = 2
        pp = 0                                  # count populations
        ll = 0                                  # count logit probabilities
        # cycle thru 2 blocks
        for i in range(nfigures):
            P.figure(i)
            lastFigure = i == (nfigures - 1)
            # plot population above probability of defoliation
            # count number of plots in figure i
            cc = 0
            # for figure i, cycle thru 2 rows
            for j in range(2):
                # if in first row, plot populations
                if j == 0:
                    # cycle thru 2 columns for populations
                    for k in range(2):
                        P.subplot(2,2,cc+1)
                        rid = self.simRow[pp]
                        cid = self.simCol[pp]
                        popsize = self.simPop[2:,rid,cid]
                        P.plot(popsize)
                        P.ylim(-2,6)
                        P.title('pop '+ str(pp))
                        P.xlabel('Year')
                        P.ylabel('log pop. size')
                        pp = pp + 1
                        cc = cc + 1
                if j == 1:
                    for k in range(2):
                        P.subplot(2,2,cc+1)
                        rid = self.simRow[ll]
                        cid = self.simCol[ll]
                        pDefol = self.probDefol[:,rid,cid]
                        P.plot(pDefol)
                        P.ylim(0,0.5)
                        P.xlabel('Year')
                        P.ylabel('Probability of defol')
                        P.axhline(y=0.35, color='k', lw=2)
                        ll = ll + 1
                        cc = cc + 1
            P.show(block=lastFigure)


            


def main(params):

    # paths and data to read in
    defolpath = os.getenv('DEFOLPROJDIR', default='.')

    inputGibbs = os.path.join(defolpath, 'out_gibbs_7.pkl')
    fileobj = open(inputGibbs, 'rb')
    gibbsobj = pickle.load(fileobj)
    fileobj.close()

    inputGibbs = os.path.join(defolpath, 'out_basicdata.pkl')
    fileobj = open(inputGibbs, 'rb')
    basicdata = pickle.load(fileobj)
    fileobj.close()


    resultsobj = ResultsProcessing(gibbsobj, basicdata, params)

#    resultsobj.makeTableFX()

#    print(resultsobj.makeTableFX())

###    resultsobj.plotFX()

#    resultsobj.plotPredFX()

#    resultsobj.writeToFileFX()


if __name__ == '__main__':
    main(params)


