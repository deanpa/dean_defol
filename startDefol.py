#!/usr/bin/env python

import sys
import defol
import params
import optparse

class CmdArgs(object):
    def __init__(self):
        p = optparse.OptionParser()
        p.add_option("--basicdata", dest="basicdata", help="Input basicdata pkl")
        p.add_option("--popdata", dest="popdata", help="Input popdata pkl")
        (options, args) = p.parse_args()
        self.__dict__.update(options.__dict__)

cmdargs = CmdArgs()
print(cmdargs.basicdata)
print(cmdargs.popdata)
        

params = params.Params()

if len(sys.argv) == 1:
    # no basicdata
    defol.main(params)

else:
    #then passed the name of the pickle file on the command line
    defol.main(params, cmdargs.basicdata, cmdargs.popdata)

