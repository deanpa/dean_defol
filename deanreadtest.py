#!/usr/bin/env python

import numpy as np
import os

defolpath = os.getenv('DEFOLPROJDIR', default='.')
# paths and data to read in
defolFname = os.path.join(defolpath,'test.txt')

defolWeath = np.genfromtxt(defolFname,  delimiter=',', names=True,
    dtype=['i8', 'i8', 'i8', 'i8', 'f8', 'f8', 'i8', 'i8', 
            'f8', 'f8', 'f8', 'f8'])


easting = defolWeath['x']
northing = defolWeath['y']
years = np.unique(defolWeath['year_'])
nYears = np.size(years)
pid = defolWeath['pid']
OBJECTID = defolWeath['OBJECTID']

print('pid', pid, 'easting', easting)
print('objid', OBJECTID)






























