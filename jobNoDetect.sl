#!/bin/bash
#SBATCH -J NoDetect
#SBATCH -A landcare00025
#SBATCH --mail-type=end
#SBATCH --mail-user=andersond@landcareresearch.co.nz
#SBATCH --time=190:00:00
#SBATCH --mem-per-cpu=3000  
#SBATCH -o job-%j.%N.out
#SBATCH -e job-%j.%N.err

source /landcare/sw/osgeo/osgeo.sh

srun startNoDetect.py --basicdata=$DEFOLPROJDIR/out_basicdata_NoDetect.pkl

#srun startNoDetect.py


